<?php

class RatesController extends Zend_Controller_Action
{
	protected $formSell;
	protected $formBuy;	
	
	//models
	protected $modelRates;
	

    public function init()
    {
       $this->formBuy = new Form_Ratebuy();
	   $this->formSell = new Form_Ratesell();
	   $this->modelRates = new Model_Rates();
	   $this->_redirector = $this->_helper->getHelper('Redirector');
	   
	  $this->_redirector = $this->_helper->getHelper('Redirector');
		
		$auth = Zend_Auth::getInstance();
		if (!$auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/account/login');
		}
		
		if($auth->getIdentity()->agentcatid!==''){
			$this->role = $auth->getIdentity()->agentcatid;
			$this->userId = $auth->getIdentity()->id;
			$this->UserAccountno = $auth->getIdentity()->accountno;
			
			//fetch shop id of the current logged in user
			if($this->role==102){
				$this->agentid = $auth->getIdentity()->agentid;
			}elseif($this->role==103){
				$this->shopid = $auth->getIdentity()->shopid;
			}
		}
		
		
		
		
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			$this->view->message =$message;
    	} 
		if(isset($_GET['error'])){
    		$mid=$_GET['error'];
			$this->view->mid =$mid;
			
    	}  
    }

    public function indexAction()
    {
       	$auth = Zend_Auth::getInstance();
		
       //fetch all buying rates
       if($this->role == 102){
       		if($this->agentid){
       			$this->view->buyingrates = $this->modelRates->fetchbuyingratesByAgent($this->agentid);
				$this->view->sellingrates = $this->modelRates->fetchSellingratesByAgent($this->agentid);
       		}
		
       }elseif($this->role == 103){
       		if($this->shopid){
       			$this->view->buyingrates = $this->modelRates->fetchBuyingratesByShop($this->shopid);
				$this->view->sellingrates = $this->modelRates->fetchSellingratesByShop($this->shopid);
       		}
       }else{
       	$this->view->buyingrates = $this->modelRates->fetchbuyingrates();
       //fetch all selling rates
       $this->view->sellingrates = $this->modelRates->fetchsellingrates();
       }
	   
       
       
       //buying rate form 
       $this->formBuy->setAction('/rates');
	   $this->formBuy->setmethod('post');
	   $formBuy = $this->formBuy;
	   $this->view->formBuy = $formBuy;
	   
       
       //selling form rate
        $this->formSell->setAction('/rates/new');
	   $this->formSell->setmethod('post');
	   $formSell = $this->formSell;
	   
	   $this->view->formSell = $formSell;
	   
	   if ($this->getRequest()->isPost()){
			//new clear form data
			if( $formBuy->isValid($_POST)){
				$data1=$formBuy->getValues();
				if(count($data1)>0){
					
					$data1['type'] = 'buying';
					if ($auth->hasIdentity()) {
						$data1['createdby'] =	$auth->getIdentity()->accountno;
						
						$data1['shopid'] = $auth->getIdentity()->shopid;
						
						$data1['agentid'] = $auth->getIdentity()->agentid;
						
						$result = $this->modelRates->addData($data1);
						
						if($result){
							$this->_redirector->gotoUrl('/rates?message=success');
						}else{
							$this->_redirector->gotoUrl('/rates?message=error');
							$this->view->error = 1;
						}
					}
					
				}
			}
		}
	   
	   
    }

    public function newAction()
    {
       $auth = Zend_Auth::getInstance();	
       $request = $this->getRequest();
		 //validate user
	   if ($this->_request->isPost()){
	   		$data = $request->getPost();
		   if(count($data)>0){
				$data['type'] = 'selling';
				if ($auth->hasIdentity()) {
					$data['createdby'] =	$auth->getIdentity()->accountno;
					
					$data['shopid'] = $auth->getIdentity()->shopid;
					
					$data['agentid'] = $auth->getIdentity()->agentid;
					
					$result = $this->modelRates->addData($data);
					
					if($result){
						$this->_redirector->gotoUrl('/rates?message=success');
					}else{
						$this->_redirector->gotoUrl('/rates?message=error');
						$this->view->error = 1;
					}
				}
				
			}
		   
		   
	   }
			
    }

    public function deleteAction()
    {
        // action body
    }

    public function adminAction()
    {
        // action body
    }

    public function buyingAction()
    {
        // action body
    }

    public function sellingAction()
    {
        // action body
    }


}











