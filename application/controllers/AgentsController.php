<?php

class AgentsController extends Zend_Controller_Action
{

    protected $formAgents = null;

    protected $modelAgents = null;

    protected $modelMembers = null;

    protected $modelPurchases = null;

    protected $modelSales = null;

    public function init()
    {
        $this->modelAgents = new Model_Agents();
		$this->modelMembers = new Model_Members();
		$this->modelPurchases = new Model_Purchase();
		$this->modelSales = new Model_Sales();
		
		$this->formAgents = new Form_Bulkingagent();
		$this->_redirector = $this->_helper->getHelper('Redirector');
		
		$auth = Zend_Auth::getInstance();
		if (!$auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/account/login');
		}
		
		if($auth->getIdentity()->agentcatid!==''){
			$this->role = $auth->getIdentity()->agentcatid;
			$this->userId = $auth->getIdentity()->id;
			$this->UserAccountno = $auth->getIdentity()->accountno;
			
			//fetch shop id of the current logged in user
			if($this->role==102){
				$this->agentid = $auth->getIdentity()->agentid;
			}elseif($this->role==103){
				$this->shopid = $auth->getIdentity()->shopid;
			}
		}
		
		
		
		
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			$this->view->message =$message;
    	} 
		if(isset($_GET['error'])){
    		$mid=$_GET['error'];
			$this->view->mid =$mid;
			
    	} 
		
		
		
    }

    public function indexAction()
    {
        if(isset($_GET['message'])){
    		$message=$_GET['message'];
			 
			if($message=='success'){
				$this->view->message = 'Success!';
			}elseif($message=='error'){
				$this->view->message = 'Error occured while saving data, contact support!';
				$this->view->error = 1;
			}
    	}    
		
			
        $agents = $this->modelAgents->fetchData();
		$this->view->agents=$agents;
		
		//fetch all agents for a given bulking agent
		
		
		$form  = $this->formAgents;
		$form->setAction('/agents');
		$form->setMethod('post');
		
		if ($this->getRequest()->isPost()){
				
			if( $form->isValid($_POST)){
				$data=$form->getValues();
				if(count($data)>0){
					$result = $this->modelAgents->addData($data);
					if($result){
						$this->_redirector->gotoUrl('/agents?message=success');
					}else{
						$this->_redirector->gotoUrl('/agents?message=error');
					}
				}else{
					$this->_redirector->gotoUrl('/agents?message=error');
				}
			}
		}
		
		
		
		$this->view->form = $form;
    }

    public function newAction()
    {
        	
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			 
			if($message=='success'){
				$this->view->message = 'Success!';
			}elseif($message=='error'){
				$this->view->message = 'Error occured while saving data, contact support!';
				$this->view->error = 1;
			}
    	}    
			
        $form  = $this->formAgents;
		$form->setAction('/agents/new');
		$form->setMethod('post');
		if ($this->getRequest()->isPost()){
				
			if( $form->isValid($_POST)){
				$data=$form->getValues();
				if(count($data)>0){
					$result = $this->modelAgents->addData($data);
					if($result){
						$this->_redirector->gotoUrl('/agents/new?message=success');
					}else{
						$this->_redirector->gotoUrl('/agents/new?message=error');
					}
				}else{
					$this->_redirector->gotoUrl('/agents/new?message=error');
				}
			}
		}
		
		
		$this->view->form = $form;
    }

    public function editAction()
    {
        	
		if(null !==($this->_request->getParam('id'))){
       		$id = $this->_request->getParam('id');	
			
	        $form  = $this->formAgents;
			$form->setAction('/agents/edit'.$id);
			$form->setMethod('post');
			if ($this->getRequest()->isPost()){
					
				if( $form->isValid($_POST)){
					$data=$form->getValues();
					if(count($data)>0){
						$result = $this->modelAgents->addData($data);
						if($result){
							$this->_redirector->gotoUrl('/agents/edit?message=success');
						}else{
							$this->_redirector->gotoUrl('/agents/edit?message=error');
						}
					}else{
						$this->_redirector->gotoUrl('/agents/new?message=error');
					}
				}
			}
			
			
			$this->view->form = $form;
		
    }
    }
    public function deleteAction()
    {
        // action body
    }

    public function detailsAction()
    {
        if(null !==($this->_request->getParam('id'))){
       		$id = $this->_request->getParam('id');
			
			$this->view->member = $this->modelAgents->fetchByGuid($id);
			
			//fetch all agents for a given bulking agent: assumed 1 for now
			$agentid = 1;
			$this->view->agents = $this->modelMembers->fetchAgentsPerBulkingAgent($agentid);
			
			
			//fetch purchases for this bulking agent
			$this->view->purchases = $this->modelPurchases->fetchPurchasesByAgentsPerBA($agentid);
			
			$this->view->sales = $this->modelSales->fetchSalesByAgentsPerBA($agentid);
			
			
		}

    }

    public function agentdetailsAction()
    {
        if(null !==($this->_request->getParam('token'))){
       		$guid = $this->_request->getParam('token');
			
			$agent = $this->modelAgents->fetchByGuid($guid);
			$agentcatid = 102;
			if($agent){
				$this->view->agent = $agent;
			}
			
			//fetch all farmers attached to this agent
			$this->view->farmers = $this->modelMembers->fetchFarmersByAgent($agent->id);
			
			
			//fetch all agents(staff) attached to this shop
			
			$this->view->staff = $this->modelMembers->fetchAgentsByBulkingAgent($agent->id);
			
			//fetch all purchase history for this agent
			
			$this->view->purchases = $this->modelPurchases->fetchPurchasesByAgentsPerBA(null,$agent->id);
			
			//fetch all sales history for this agent
			
			$this->view->sales = $this->modelSales->fetchSalesByAgentsPerBA($agent->id);
			
		}
    }


}











