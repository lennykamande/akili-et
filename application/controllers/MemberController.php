<?php

class MemberController extends Zend_Controller_Action
{

    protected $modelMembers = null;

    protected $modelNyumbaKumi = null;

    protected $modelMailing = null;

    protected $modelSMS = null;

    protected $modelAgents = null;

    protected $modelInputShops = null;

    protected $modelShops = null;

    protected $modelPurchases = null;

    protected $modelSales = null;

    protected $modelPayments = null;

    protected $formFarmer = null;

    protected $formNyumbaKumi = null;

    protected $formCustomer = null;

    public function init()
    {
        	
        $this->modelMembers = new Model_Members();
		$this->modelPurchases = new Model_Purchase();
		$this->modelNyumbaKumi = new Model_Nyumbakumi();
		$this->modelAgents = new Model_Agents();
		$this->modelShops= new Model_Shops();
		$this->modelInputShops = new Model_Inputshops();
		$this->modelPayments = new Model_Payments();
		$this->modelSales = new Model_Sales();
		$this->modelSMS = new Model_Sms();
		$this->modelMailing = new Model_Mailing();
		
		//new farmer form
		$this->formFarmer= new Form_Farmer();
		$this->formFarmer->setAction('/member/new-farmer/');
		$this->formFarmer->setMethod('post');
		
		
		//nyumba kumi form
		$this->formNyumbaKumi = new Form_Farmergroup();
		$this->formNyumbaKumi->setAction('/member/new-farmer-group');
		$this->formNyumbaKumi->setmethod('post');
		
		$this->view->formNyumbaKumi = $this->formNyumbaKumi;
		
		$this->_redirector = $this->_helper->getHelper('Redirector');
		
		
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			if($message=='success'){
				$this->view->message = 'Success!';
			}elseif($message=='error'){
				$this->view->message = 'Error occured while saving data, please countercheck your dota to ensure they are valid!';
				$this->view->error = 1;
			}
    	}
		
		
		$auth = Zend_Auth::getInstance();
		if (!$auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/account/login');
		}
		
		
		if($auth->getIdentity()->agentcatid!==''){
			$this->role = $auth->getIdentity()->agentcatid;
			$this->userId = $auth->getIdentity()->id;
			$this->UserAccountno = $auth->getIdentity()->accountno;
		}
		if($this->role==102){ // if agent
			
		}
		
		
		
		
		if($auth->getIdentity()->agentcatid!==''){
			$this->role = $auth->getIdentity()->agentcatid;
			
			$this->userId = $auth->getIdentity()->id;
			
			if($auth->getIdentity()->agentid!==''){
				$this->agentid = $auth->getIdentity()->agentid;
			}
			
			if($auth->getIdentity()->shopid!==''){
				$this->shopid = $auth->getIdentity()->shopid;
			}
			
			if($auth->getIdentity()->inputshopid!==''){
				$this->inputshopid = $auth->getIdentity()->inputshopid;
			}
			
			if($auth->getIdentity()->agentid!==''){
				$this->agentid = $auth->getIdentity()->agentid;
			}
			
			$this->guid = $auth->getIdentity()->guid;
			$this->UserAccountno = $auth->getIdentity()->accountno;
			
		}
		
		
    }

    public function indexAction()
    {
        // action body
        
         $this->view->farmers  = $this->modelMembers->fetchFarmers();
		 
		 if($this->role == 102){
		 	//fetch agents of this shop only
		 	$agentid =$this->agentid;
			
			$this->view->agents = $this->modelMembers->fetchAgentsByBulkingAgent($agentid);
			$this->view->customers = $this->modelMembers->fetchCustomersByBulkingAgent($agentid);
			
		 }else{
		 	$this->view->agents = $this->modelMembers->fetchBulkingAgents();
		 }
		 
		 if($this->role == 103){
		 	//fetch agents of this shop only
		 	$shopid = $this->shopid;
			
			$this->view->clerks = $this->modelMembers->fetchClerksByShop($shopid);
			$this->view->customers = $this->modelMembers->fetchCustomersByShop($shopid);
			
		 }else{
		 	$this->view->clerks = $this->modelMembers->fetchShopAttendants();
		 }
		 
		 $this->view->providers = $this->modelMembers->fetchInputShopMembers();
		 $this->view->administrators  = $this->modelMembers->fetchAdministrators(); 
		 if($this->role==106){
		 	$this->view->customers = $this->modelMembers->fetchCustomers();
		 }
		 
		 
		$formCustomer = new Form_Customer();
		$formCustomer->setAction('/member/customers');
		$formCustomer->setMethod('post');
		
		$this->view->customerform = $formCustomer;
		
    }

    public function newAction()
    {
        // action body
    }

    public function farmersAction()
    {
        $this->view->farmers  = $this->modelMembers->fetchFarmers();
		
		
		//new nyumbakumi form
		
		
    }

    public function providersAction()
    {
         $this->view->members = $this->modelMembers->fetchInputShopMembers();
		 
		
    }

    public function shopAttendantsAction()
    {
    	
        $this->view->members = $this->modelMembers->fetchShopAttendants();
		
		
    }

    public function customersAction()
    {
     	$this->view->members = $this->modelMembers->fetchCustomers();
		
		$formCustomer = new Form_Customer();
		$formCustomer->setAction('/member/customers');
		$formCustomer->setMethod('post');
		
		
		if ($this->getRequest()->isPost()){
				
			//new clear form data
			if( $formCustomer->isValid($_POST)){
				$data=$formCustomer->getValues();
				if(count($data)>0){
						
					//break the full name recieved
				
					$accountArray = explode(' ', $data['names']);
					
					if(isset($accountArray['0'])){
						$data['fname'] =  $accountArray['0'];
					}
					if(isset($accountArray['1'])){
						$data['mname'] =  $accountArray['1'];
					}
					if(isset($accountArray['2'])){
						$data['lname'] =  $accountArray['2'];
					}
					
					$data['accountno'] = $this->modelMembers->generateAndValidateAccountNo();
					if(isset($data['accountno'])){
						$data['activate'] = md5(uniqid('et'));
					}
					$data['orgid']  = 1;
					$data['agentcatid']  = 105;
					
					$emailexists = $this->modelMembers->checkifaccountexists($data['email'] , $data['phoneno'] );
				
					if(!$emailexists){
						//save the data in the database
						$id = $this->modelMembers->addData($data);
						if($id){
							$member = $this->modelMembers->fetchById($id);
							
							//send email
							$to = $data['email'];
							$subject = 'Akili eT: Account created';
							
							$msg = 'Hello, <br/><p>An Akili eT has been created for you.<p>Akili eT Account No.: <b>'.$member->accountno.'</b></p></p>
									<p>To login, go to 
									<a href="'.DOMAINNAME.'/member/create-password/'.$data['activate'].'">'.DOMAINNAME.'/member/create-password/'.$data['activate'].' </a></p>
									<p>Thanks for joining Akili eT.</p><br/>
									<p>-- <br/> Akili eT Team.</p>';
									
									
									
							//send mail to the farmer
							$mail = $this->modelMailing->sendmail(NULL, $to, $msg, $subject);
							
							//send sms to farmer
							
							$toPhone = $this->modelSMS->formatPhoneno($data['phoneno']);
							
							//$message = 'Akili eT: Account has been created. Account No.: '.$member->accountno.'. Thanks';
							$message = 'AKili eT: An Account has been created for your on '.DOMAINNAME.'. Your Account No. is : '.$member->accountno.'. Check your email('.$member->email.') for instructions on how to login';
							
							if($toPhone){
								$sms  = $this->modelSMS->sendSMS($toPhone, $message);
							}
							
							if($id){
									$this->_redirector->gotoUrl('/member/customers?message=success');
									
							}else{
								$this->_redirector->gotoUrl('/member/customers?message=error');
								$this->view->error = 1;
							}
						}else{
							$this->view->message = 'Error occured recording data, please contact support!';
							$this->view->error = 1;
						}
						
						
						//send email
						
						//send sms to the agent
					
					}else{
						$this->view->message = 'Email or Phone already registered!';
						$this->view->error = 1;
					}
				}
			}else{
				$this->_redirector->gotoUrl('/member/customers?message=error');
			}
		}
		
		$this->view->form = $formCustomer;
		
		
		
		
    }

    public function bulkingAgentsAction()
    {
       $this->view->members = $this->modelMembers->fetchBulkingAgents();
    }
    public function administratorAction()
    {
         $this->view->members = $this->modelMembers->fetchAdministrators();
		 
		
    }

    public function farmerGroupsAction()
    {
        // action body
    }

    public function newFarmerAction()
    {
		if($this->role==102){
			$this->formFarmer->removeElement('parentagentid');
		}	
        $this->view->form = $this->formFarmer;
		
		 if($this->_request->isPost()){
			$data = $this->_request->getParams();
			 if(count($data)>0){
			 		
					
			 	//break the full name recieved
				$accountArray = explode(' ', $data['names']);
				
				if(isset($accountArray['0'])){
					$data['fname'] =  $accountArray['0'];
				}
				if(isset($accountArray['1'])){
					$data['mname'] =  $accountArray['1'];
				}
				if(isset($accountArray['2'])){
					$data['lname'] =  $accountArray['2'];
				}
				if($this->role==102){
					$data['parentagentid'] = $this->agentid;
				}
				
				$data['accountno'] = $this->modelMembers->generateAndValidateAccountNo();
				
				$data['guid'] = uniqid('activate');
				if(isset($data['accountno'])){
					$data['activate'] = md5($data['guid']);
				}
				
				$data['orgid']  = 1;
				$data['agentcatid']  = 101;
				
				
				$accountexists = $this->modelMembers->checkifaccountexists($data['email'], $data['phoneno']);
				
				if(!$accountexists){
						
					$addedaccountid = $this->modelMembers->addData($data);
					
					$member=$this->modelMembers->fetchById($addedaccountid);
					
					//send email
					$to = $data['email'];
					$subject = 'Akili eT: Account created';
					
					$msg = 'Hello, <br/><p>An Akili eT has been created for you.<p>Akili eT Account No.: <b>'.$member->accountno.'</b></p></p>
							<p>To login, go to 
							<a href="'.DOMAINNAME.'/member/create-password/'.$data['activate'].'">'.DOMAINNAME.'/member/create-password/'.$data['activate'].' </a></p>
							<p>Thanks for joining Akili eT.</p><br/>
							<p>-- <br/> Akili eT Team.</p>';
					
					
					
					//send mail to the farmer
					$mail = $this->modelMailing->sendmail(NULL, $to, $msg, $subject);
					//send sms to farmer
					
					$toPhone = $this->modelSMS->formatPhoneno($data['phoneno']);
					$message = 'AKili eT: An Account has been created for your on '.DOMAINNAME.'. Your Account No. is : '.$member->accountno.'. Check your email('.$member->email.') for instructions on how to login';
					
					if($toPhone){
						$sms  = $this->modelSMS->sendSMS($toPhone, $message);
					}
					
					if($addedaccountid){
							$this->view->message = 'Success!';
							
					}else{
						$this->view->message = 'Error occured recording data, please contact support!';
							$this->view->error = 1;
					}
				}else{
					$this->view->message = 'An account with the same email or phone no has been registered before. Try another';
				$this->view->error = 1;
				}
				
			 }else{
			 	$this->view->message = 'No data recieved!';
				$this->view->error = 1;
			 }
   		
    
    
    
    
    
    
    }
    }

    public function newFarmerGroupAction()
    {
       if($this->_request->isPost()){
			$data = $this->_request->getParams();
		   if(count($data)>0){
		   	 $result = $this->modelNyumbaKumi->addData($data);
			   if($result){
			   	$this->view->message = 'Success!';
			   }else{
			   	$this->view->message = 'Error occured, try again!';
				$this->view->error = 1;
			   }
		   }else{
		   	$this->view->message = 'No data recieved';
			$this->view->error = 1;
		   }
    	
    
    
    
    
    
    
    }
	}
    public function newBulkingAgentAction()
    {
    	$formAgent = new Form_Bulkingagent();
		$formAgent->setAction('/member/new-bulking-agent');
		$formAgent->setMethod('post');
		
		
		
		 if($this->_request->isPost()){
			$data = $this->_request->getParams();
			 if(count($data)>0){
			 	$data['orgid']= 1;
				
				$result = $this->modelAgents->addData($data);
				if($result){
					$this->view->message = 'Success!';
				}else{
					$this->view->message = 'Error occured, try again!';
					$this->view->error = 1;
				}
			 }
		 }
		 $this->view->formAgent = $formAgent;
    }

    public function newProviderAction()
    {
        	
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			 
			if($message=='success'){
				$this->view->message = 'Success!';
			}elseif($message=='error'){
				$this->view->message = 'Error occured while saving data, contact support!';
				$this->view->error = 1;
			}
    	}   
		
			
        $formProvider = new Form_Provider();
		$formProvider->setAction('/member/new-provider');
		$formProvider->setMethod('post');
		
		 $formInputshop  = new Form_Inputshop();
		 $formInputshop->setAction('/member/new-provider');
		$formInputshop->setMethod('post');
		
		if ($this->getRequest()->isPost()){
				
			//new clear form data
			if( $formProvider->isValid($_POST)){
				$data=$formProvider->getValues();
				
				if(count($data)>0){
						
					//break the full name recieved
				
					$accountArray = explode(' ', $data['names']);
					
					if(isset($accountArray['0'])){
						$data['fname'] =  $accountArray['0'];
					}
					if(isset($accountArray['1'])){
						$data['mname'] =  $accountArray['1'];
					}
					if(isset($accountArray['2'])){
						$data['lname'] =  $accountArray['2'];
					}
					
					
					$data['accountno'] = $this->modelMembers->generateAndValidateAccountNo();
					if(isset($data['accountno'])){
						$data['activate'] = md5($data['accountno']);
					}
					$data['orgid']  = 1;
					$data['agentcatid']  = 104;
					
					$emailexists = $this->modelMembers->checkifaccountexists($data['email'] , $data['phoneno'] );
				
					if(!$emailexists){
						//save the data in the database
						$id = $this->modelMembers->addData($data);
						if($id){
							$member = $this->modelMembers->fetchById($id);
							
							//send email
							$to = $data['email'];
							$subject = 'Akili eT: Account created';
							
							$msg = 'Hello, <br/><p>An Akili eT has been created for you.<p>Akili eT Account No.: <b>'.$member->accountno.'</b></p></p>
									<p>To login, go to 
									<a href="'.DOMAINNAME.'/member/create-password/'.$data['activate'].'">'.DOMAINNAME.'/member/create-password/'.$data['activate'].' </a></p>
									<p>Thanks for joining Akili eT.</p><br/>
									<p>-- <br/> Akili eT Team.</p>';
									
									
									
							//send mail to the farmer
							$mail = $this->modelMailing->sendmail(NULL, $to, $msg, $subject);
							
							//send sms to farmer
							
							$toPhone = $this->modelSMS->formatPhoneno($data['phoneno']);
							
							//$message = 'Akili eT: Account has been created. Account No.: '.$member->accountno.'. Thanks';
							$message = 'AKili eT: An Account has been created for your on '.DOMAINNAME.'. Your Account No. is : '.$member->accountno.'. Check your email('.$member->email.') for instructions on how to login';
							
							if($toPhone){
								$sms  = $this->modelSMS->sendSMS($toPhone, $message);
							}
							
							if($id){
									$this->_redirector->gotoUrl('/member/new-provider?message=success');
									
							}else{
								$this->_redirector->gotoUrl('/member/new-provider?message=error');
								$this->view->error = 1;
							}
						}else{
							$this->view->message = 'Error occured recording data, please contact support!';
							$this->view->error = 1;
						}
						
						
						//send email
						
						//send sms to the agent
					
					}else{
						$this->view->message = 'Email or Phone already registered!';
						$this->view->error = 1;
					}
				}
			}
			
			//new clear form data
			if( $formInputshop->isValid($_POST)){
				$dataInputShops=$formInputshop->getValues();
				
				if(count($dataInputShops)>0){
					$result = $this->modelInputShops->addData($dataInputShops);
					if($result){
						$this->_redirector->gotoUrl('/member/new-provider?message=success');
					}else{
						$this->_redirector->gotoUrl('/member/new-provider?message=error');
					}
				}else{
					$this->_redirector->gotoUrl('/member/new-provider?message=error');
				}
				
			}
		}
		
		
		
		$this->view->formProvider = $formProvider;
		$this->view->formInputshop = $formInputshop;
    }

    public function newAgentAction()
    {
        $formAgent = new Form_Agent();
		$formAgent->setAction('/member/new-agent');
		$formAgent->setMethod('post');
		
		if($this->_request->isPost()){
			$data = $this->_request->getParams();
			if(count($data)>0){
			 		
			 	//break the full name recieved
				
				$accountArray = explode(' ', $data['names']);
				
				if(isset($accountArray['0'])){
					$data['fname'] =  $accountArray['0'];
				}
				if(isset($accountArray['1'])){
					$data['mname'] =  $accountArray['1'];
				}
				if(isset($accountArray['2'])){
					$data['lname'] =  $accountArray['2'];
				}
				
				$data['accountno'] = $this->modelMembers->generateAndValidateAccountNo();
				if(isset($data['accountno'])){
					$data['activate'] = md5(uniqid('et'));
				}
				$data['orgid']  = 1;
				$data['agentcatid']  = 102;
				
				//check if phone no or email has been registered
				
				$emailexists = $this->modelMembers->checkifaccountexists($data['email'] , $data['phoneno'] );
				
				if(!$emailexists){
					//save the data in the database
					$id = $this->modelMembers->addData($data);
					if($id){
						$member = $this->modelMembers->fetchById($id);
						
						//send email
						$to = $data['email'];
						$subject = 'Akili eT: Account created';
						
						$msg = 'Hello, <br/><p>An Akili eT has been created for you.<p>Akili eT Account No.: <b>'.$member->accountno.'</b></p></p>
								<p>To login, go to 
								<a href="'.DOMAINNAME.'/member/create-password/'.$data['activate'].'">'.DOMAINNAME.'/member/create-password/'.$data['activate'].' </a></p>
								<p>Thanks for joining Akili eT.</p><br/>
								<p>-- <br/> Akili eT Team.</p>';
								
								
								
						//send mail to the farmer
						$mail = $this->modelMailing->sendmail(NULL, $to, $msg, $subject);
						
						//send sms to farmer
						
						$toPhone = $this->modelSMS->formatPhoneno($data['phoneno']);
						
						$message = 'AKili eT: An Account has been created for your on '.DOMAINNAME.'. Your Account No. is : '.$member->accountno.'. Check your email('.$member->email.') for instructions on how to login';
						
						if($toPhone){
							$sms  = $this->modelSMS->sendSMS($toPhone, $message);
						}
						
						if($id){
								$this->view->message = 'Success!';
								
						}else{
							$this->view->message = 'Error occured recording data, please contact support!';
								$this->view->error = 1;
						}
					}else{
						$this->view->message = 'Error occured recording data, please contact support!';
						$this->view->error = 1;
					}
					
					
					//send email
					
					//send sms to the agent
				
				}else{
					$this->view->message = 'Email or Phone already registered!';
					$this->view->error = 1;
				}
				
				
				
				
				
				
			}else{
			 	$this->view->message = 'No data recieved!';
				$this->view->error = 1;
			 }
			
		}
		
		$this->view->formAgent = $formAgent;
    }

    public function newClerkAction()
    {
    	if(isset($_GET['message'])){
    		$message=$_GET['message'];
			 
			if($message=='success'){
				$this->view->message = 'Success!';
			}elseif($message=='error'){
				$this->view->message = 'Error occured while saving data, contact support!';
				$this->view->error = 1;
			}
    	}    	
		
		
			
        $formZiwaShop = new Form_Shops();
		$formZiwaShop->setAction('/member/new-clerk');
		$formZiwaShop->setMethod('post');
		
        $formClerks = new Form_Clerk();
		$formClerks->setAction('/member/new-clerk');
		$formClerks->setMethod('post');
		
		
		if ($this->getRequest()->isPost()){
				
			//new clear form data
			if( $formClerks->isValid($_POST)){
				$data=$formClerks->getValues();
				if(count($data)>0){
						
					//break the full name recieved
				
					$accountArray = explode(' ', $data['names']);
					
					if(isset($accountArray['0'])){
						$data['fname'] =  $accountArray['0'];
					}
					if(isset($accountArray['1'])){
						$data['mname'] =  $accountArray['1'];
					}
					if(isset($accountArray['2'])){
						$data['lname'] =  $accountArray['2'];
					}
					
					
					$data['accountno'] = $this->modelMembers->generateAndValidateAccountNo();
					if(isset($data['accountno'])){
						$data['activate'] = md5($data['accountno']);
					}
					$data['orgid']  = 1;
					$data['agentcatid']  = 103;
					
					
					$emailexists = $this->modelMembers->checkifaccountexists($data['email'] , $data['phoneno'] );
				
					if(!$emailexists){
						//save the data in the database
						$id = $this->modelMembers->addData($data);
						if($id){
							$member = $this->modelMembers->fetchById($id);
							
							//send email
							$to = $data['email'];
							$subject = 'Akili eT: Account created';
							
							$msg = 'Hello, <br/><p>An Akili eT has been created for you.<p>Akili eT Account No.: <b>'.$member->accountno.'</b></p></p>
									<p>To login, go to 
									<a href="'.DOMAINNAME.'/member/create-password/'.$data['activate'].'">'.DOMAINNAME.'/member/create-password/'.$data['activate'].' </a></p>
									<p>Thanks for joining Akili eT.</p><br/>
									<p>-- <br/> Akili eT Team.</p>';
									
									
									
							//send mail to the farmer
							$mail = $this->modelMailing->sendmail(NULL, $to, $msg, $subject);
							
							//send sms to farmer
							
							$toPhone = $this->modelSMS->formatPhoneno($data['phoneno']);
							
							//$message = 'Akili eT: Account has been created. Account No.: '.$member->accountno.'. Thanks';
							$message = 'AKili eT: An Account has been created for your on '.DOMAINNAME.'. Your Account No. is : '.$member->accountno.'. Check your email('.$member->email.') for instructions on how to login';
							
							if($toPhone){
								$sms  = $this->modelSMS->sendSMS($toPhone, $message);
							}
							
							if($id){
									$this->_redirector->gotoUrl('/member/new-clerk?message=success');
									
							}else{
								$this->_redirector->gotoUrl('/member/new-clerk?message=error');
								$this->view->error = 1;
							}
						}else{
							$this->view->message = 'Error occured recording data, please contact support!';
							$this->view->error = 1;
						}
						
						
						//send email
						
						//send sms to the agent
					
					}else{
						$this->view->message = 'Email or Phone already registered!';
						$this->view->error = 1;
					}

					
				}
			}
			
			//new shop form data
			if( $formZiwaShop->isValid($_POST)){
				//new shop	
				$shopdata=$formZiwaShop->getValues();
				
				if(count($shopdata)>0){
					$shopdata['orgid']=1;
					$result = $this->modelShops->addData($shopdata);
					if($result){
						$this->_redirector->gotoUrl('/member/new-clerk?message=success');
					}else{
						$this->_redirector->gotoUrl('/member/new-clerk?message=error');
					}
					//show the error or success upon submitting data
				}
			}
		}
		
		$this->view->formClerks = $formClerks;
		$this->view->formShops= $formZiwaShop;
    }

    public function newCustomerAction()
    {
        // action body
    }

    public function farmerdetailsAction()
    {
       	if(null !==($this->_request->getParam('accountno'))){
       		$accountno = $this->_request->getParam('accountno');
			
		   $this->view->member = $this->modelMembers->fetchByAccountNo($accountno);
		   //fetch deliveries
		   $this->view->purchases = $this->modelPurchases->fetchPurchasesByAccountno($accountno);
	  
	   		//fetch paymentss made to him
	   		$this->view->payments = $this->modelPayments->fetchPaymentsByPayee($accountno);
       	}
       
    }

    public function editfarmerAction()
    {
        if(null !==($this->_request->getParam('accountno'))){
       		$accountno = $this->_request->getParam('accountno');
			
			$member = $this->modelMembers->fetchByAccountNo($accountno);
			
			$formFarme = new Form_Editfarmer();
			$formFarme->setAction('/member/editfarmer/'.$accountno);
			$formFarme->setMethod('post');
			
			$formFarme->populate($member->toArray());
			
			 if($this->_request->isPost()){
				$data = $this->_request->getParams();
				 if(count($data)>0){
				 		
				 	//break the full name recieved
					
					$result = $this->modelMembers->updateData($accountno, $data);
					
					if($result){
						$formFarme->populate($member->toArray());
							$this->_redirector->gotoUrl('/members/farmers');
						
					}else{
						$this->view->message = 'Error occured recording data, please contact support!';
							$this->view->error = 1;
					}
					
				 }else{
				 	$this->view->message = 'No data recieved!';
					$this->view->error = 1;
				 }
	    
	    	}
			 
			 $this->view->formfarmer = $formFarme;
			
       	
    
    
    
    
    }
    }
    public function deleteAction()
    {
        if(null !==($this->_request->getParam('accountno'))){
       		$accountno = $this->_request->getParam('accountno');
			$data= array();
			$data['deleted'] = 1;
			$data['deletedby'] = 1;
			$data['active'] = 0;
			
			$result = $this->modelMembers->updateData($accountno, $data);
			if($result){
				$this->_redirector->gotoUrl('/members/farmers?message=success');
			}else{
				$this->_redirector->gotoUrl('/members/farmers?message=error');
			}
		
    
    
    
    }
	}
    public function bulkingAgentDetailsAction()
    {
        if(null !==($this->_request->getParam('accountno'))){
       		$accountno = $this->_request->getParam('accountno');
			
		   $this->view->member = $this->modelMembers->fetchByAccountNo($accountno);
		   
		   //fetch deliveries
		   $this->view->purchases = $this->modelPurchases->fetchPurchasesByAccountno($accountno);
		   
	  
	   		//fetch all sales made by this agent
	   		$this->view->sales = $this->modelSales->fetchSalesByAccountno($accountno);
       	
    
    
    }
	}
    public function editagentAction()
    {
       if(null !==($this->_request->getParam('accountno'))){
       		$accountno = $this->_request->getParam('accountno');
			
			$member = $this->modelMembers->fetchByAccountNo($accountno);
			
			$formFarme = new Form_Editbulkingagent();
			$formFarme->setAction('/member/editagent/'.$accountno);
			$formFarme->setMethod('post');
			
			$formFarme->populate($member->toArray());
			
			 if($this->_request->isPost()){
				$data = $this->_request->getParams();
				 if(count($data)>0){
				 		
				 	//break the full name recieved
					
					$result = $this->modelMembers->updateData($accountno, $data);
					
					if($result){
						$formFarme->populate($member->toArray());
							$this->_redirector->gotoUrl('/members/bulking-agents');
						
					}else{
						$this->view->message = 'Error occured recording data, please contact support!';
							$this->view->error = 1;
					}
					
				 }else{
				 	$this->view->message = 'No data recieved!';
					$this->view->error = 1;
				 }
	    
	    	}
			 
			 $this->view->formfarmer = $formFarme;
    
   		
    }
    }
    public function createPasswordAction()
    {
        // action body
    }

    public function detailsAction()
    {
     	if(null !==($this->_request->getParam('accountno'))){
       		$accountno = $this->_request->getParam('accountno');
			
		   $member = $this->modelMembers->fetchByAccountNo($accountno);
		   
		   $this->view->member = $member;
		   
		   if($member->agentcatid==101){
		   	 //if looking for farmer then fetch from db where seller = current accountmember clicked
		   	 	$this->view->sales = $this->modelPurchases->fetchPurchasesBySellerAccount($accountno);
		   }elseif($member->agentcatid==105){
		   		$this->view->sales = $this->modelSales->fetchSalesToAccountno($accountno);
		   }else{
		   		$this->view->sales = $this->modelSales->fetchSalesByAccountno($accountno);
		   }
		   //fetch deliveries
		   $this->view->purchases = $this->modelPurchases->fetchPurchasesByAccountno($accountno);
		   
		   
	   		//fetch paymentss made to him
	   		if($member->agentcatid==101){
	   			$this->view->payments = $this->modelPayments->fetchPaymentsByPayee($accountno);
	   		}elseif($member->agentcatid==102){
	   			$this->view->payments = $this->modelPayments->fetchPaymentsByAgentsPerBA(null,$member->agentid);
	   		}elseif($member->agentcatid==103){
	   			$this->view->payments = $this->modelPayments->fetchPaymentsByClerksPerShop($member->shopid);
	   		}elseif($member->agentcatid==104){
	   			$this->view->payments = $this->modelPayments->fetchExchangesPerpayer($accountno);
	   		}else{
	   			$this->view->payments = $this->modelPayments->fetchData();
	   		}
			
			
			if( $member->agentcatid==104){
	   			$this->view->exchanges = $this->modelPayments->fetchExchangesPerpayer($accountno);
	   		}elseif($member->agentcatid==101){
	   			$this->view->exchanges = $this->modelPayments->fetchExchangesByPayee($accountno);
				
	   		}
			
			
			
			//fetch changes
	   		
       	}
	    
    }


}











































