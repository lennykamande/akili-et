<?php

class HubController extends Zend_Controller_Action
{

    public function init()
    {
    	$this->_redirector = $this->_helper->getHelper('Redirector');
		
		$this->modelPurchases = new Model_Purchase();
		$this->modelSales = new Model_Sales();
		$this->modelExpenses = new Model_Expenses();
		$this->modelPayments = new Model_Payments();
		
		$auth = Zend_Auth::getInstance();
		if (!$auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/account/login');
		}
		
		if($auth->getIdentity()->agentcatid!==''){
			$this->role = $auth->getIdentity()->agentcatid;
			$this->userId = $auth->getIdentity()->id;
			$this->UserAccountno = $auth->getIdentity()->accountno;
			
			//fetch shop id of the current logged in user
			if($this->role==102){
				$this->agentid = $auth->getIdentity()->agentid;
			}elseif($this->role==103){
				$this->shopid = $auth->getIdentity()->shopid;
			}
		}
		
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			$this->view->message =$message;
    	} 
		if(isset($_GET['error'])){
    		$mid=$_GET['error'];
			$this->view->mid =$mid;
			
    	} 
		
		
    }

    public function indexAction()
    {
    	if($this->role==101){
    		
    	}elseif($this->role==102){
    		//purchases	
    		$purchase = $this->modelPurchases->fetchOneSumPurchasesByAgentsPerBA(null, $this->agentid);
			if($purchase){
				$purchaseworth = $purchase->value;
			}else{
				$purchaseworth = 0;
			}
			
			$this->view->purchaseworth = $purchaseworth;
			
			//sales
			$sale = $this->modelSales->fetchOneSumPurchasesByAgentsPerBA(null,$this->agentid);
			if($sale){
				$salesworth = $sale->value; 
			}else{
				$salesworth= 0;
			}
			$this->view->salesworth = $salesworth;
			
			//expenses
			$expenses = $this->modelExpenses->fetchSumExpensesPerBulkingAgent(102, $this->agentid);
			
			if($expenses){
				$expenseworth = $expenses->value;
			}else{
				$expenseworth = 0;
			}
			$this->view->expenseworth = $expenseworth;
			
			
			//profits
			$liability = ($purchaseworth * 1) + ($expenseworth*1);
			
			$assets = $salesworth + 0;
			$profits = $assets - $liability;
			
			if($profits){
				$this->view->profits=$profits;
			}else{
				$this->view->profits = 0;
			}
			
    	}elseif($this->role==103){
    		
			//purchases	
			
			$purchase = $this->modelPurchases->fetchOneSumPurchasesPerShop(null,$this->shopid);
			if($purchase){
				$purchaseworth = $purchase->value;
			}else{
				$purchaseworth = 0;
			}
			$this->view->purchaseworth = $purchaseworth;
			
			//sales
			$sale = $this->modelSales->fetchOneSumPurchasesPerShop(null,$this->shopid);
			if($sale){
				$salesworth = $sale->value; 
			}else{
				$salesworth= 0;
			}
			$this->view->salesworth = $salesworth;
			//expenses
			$expenses = $this->modelExpenses->fetchSumExpensesPerZiwaShop($this->shopid);
			
			if($expenses){
				$expenseworth = $expenses->value;
			}else{
				$expenseworth = 0;
			}
			$this->view->expenseworth = $expenseworth;
			
			//profits
			$liability = ($purchaseworth * 1) + ($expenseworth*1);
			
			$assets = $salesworth + 0;
			$profits = $assets - $liability;
			
			if($profits){
				$this->view->profits=$profits;
			}else{
				$this->view->profits = 0;
			}
    		
    	}elseif($this->role==104){
    		
			
			//purchases	
			
			//sales
			
			
			//expenses
			
			//profits
			
    	}elseif($this->role==105){
    		
			//purchases	
			
			//sales
			
			
			//expenses
			
			//profits
			
    	}else{
    		//if admin then query all data about all users
    		//purchases	
			$purchase = $this->modelPurchases->fetchAllSumPurchaseWorth();
			if($purchase){
				$purchaseworth = $purchase->value;
			}else{
				$purchaseworth = 0;
			}
			$this->view->purchaseworth = $purchaseworth;
			
			//sales
			$sale = $this->modelSales->fetchAllSumSalesWorth();
			if($sale){
				$salesworth = $sale->value; 
			}else{
				$salesworth= 0;
			}
			$this->view->salesworth = $salesworth;
			//expenses
			$expenses = $this->modelExpenses->fetchAllSumExpenses();
			
			if($expenses){
				$expenseworth = $expenses->value;
			}else{
				$expenseworth = 0;
			}
			$this->view->expenseworth = $expenseworth;
			
			//profits
			$liability = ($purchaseworth * 1) + ($expenseworth*1);
			
			$assets = $salesworth + 0;
			$profits = $assets - $liability;
			
			if($profits){
				$this->view->profits=$profits;
			}else{
				$this->view->profits = 0;
			}
    		
    	}
    	
    }


}

