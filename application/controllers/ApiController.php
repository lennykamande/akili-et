<?php

class ApiController extends Zend_Controller_Action {

	public function init() {
		/* Initialize action controller here */
	}

	public function loginAction() {

		$request = $this -> getRequest();
		$email = $request -> GetParam('email');
		$password = $request -> getParam('password');
		$dbAdapter = Zend_Db_Table::getDefaultAdapter();
		$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

		$authAdapter -> setTableName('users') -> setIdentityColumn('email') -> setCredentialColumn('password') -> setCredentialTreatment('MD5(?)');

		$authAdapter -> setIdentity($email) -> setCredential($password);

		$auth = Zend_Auth::getInstance();
		$result = $auth -> authenticate($authAdapter);
		if ($result -> isValid()) {

			$userInfo = $authAdapter -> getResultRowObject(null, 'password');
			$response["user"]["email"] = $userInfo -> email;
			$response["user"]["agentid"]=$userInfo->agentid;
			$response["user"]["account_id"] = $userInfo -> accountno;
			$response["success"] = 1;
			echo json_encode($response);
			exit ;
		} else {

			$response["error"] = 1;
			$response["success"] = 0;
			$response["error_msg"] = "Incorrect email or password!";
			echo json_encode($response);
			exit ;
		}

	}

	public function addproduceAction() {
				$this->_modelPurchases = new Model_Purchase();
				$this->_modelMembers = new Model_Members();
				$this->modelSMS = new Model_Sms();
				$this->modelRates  = new Model_Rates();
				
				$request = $this -> getRequest();
				$data['quantity'] = $request -> getParam('quantity');
				$data['buyerAccount'] = $request -> getParam('buyerAccount');
				$data['sellerAccount'] = $request -> getParam('sellerAccount');
				$data['agentid']=$request->getParam('agentid');
				
		
			//get the latest buying rate for the person logged in;
				if($data['agentid']){
					$buyingRate = $this->modelRates->fetchLatestAgentbuyingRate($data['agentid']);
					if($buyingRate){
						$this->buyingRate = $buyingRate->rate;
					}else{
						$this->buyingRate=null;
						
					}
					
				}
				$data['rate'] = $this->buyingRate;
				if($data['rate']){
					$data['value'] = $this->_modelPurchases->calculateValue($data['rate'], $data['quantity']);
				}
				
				if($data['value']>0){
						 		
						 	//check if account no exists	
						if($this->_modelMembers->checkIfExists($data['sellerAccount'])){
							 	try{
							 		$result=$this->_modelPurchases->addData($data);
									$phone = $this -> _modelMembers -> fetchByAccountNo($data['sellerAccount']);
										if ($phone -> phoneno !== '') {
											$message = 'AKILI eT: ' . $data['quantity'] . ' litres of milk received at ' . $data['rate'] . ' per litre. Thanks for using Akili eT.';
											$res = $this -> modelSMS -> sendSMS($phone -> phoneno, $message);
											}
											$response["success"] = 1;
											$response["error"]=0;
											$response["message"]="success";
											echo json_encode($response);
											exit;
							 		}
									catch (Exception $e){
										$response["success"] = 0;
										$response["error"]=1;
										$response["message"]="Error occured saving data to the server, please consult IT Support";
										echo json_encode($response);
										exit;
										
									}
					
						}else{
							$response["success"] = 0;
							$response["error"]=1;
							$response["message"]="The Seller is not recognized by the system";
							echo json_encode($response);
								exit;		
						}
				}else{
					$response["success"] = 0;
							$response["error"]=1;
							$response["message"]="Quantity not recieved";
							echo json_encode($response);
							exit;
				}

}
}
