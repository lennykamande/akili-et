<?php

class PaymentController extends Zend_Controller_Action
{

    protected $ModelProvides = null;

    public function init()
    {
        $this->ModelProvides = new Model_Provides();
		
		
		$auth = Zend_Auth::getInstance();
		if (!$auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/account/login');
		}
		
		if($auth->getIdentity()->agentcatid!==''){
			$this->role = $auth->getIdentity()->agentcatid;
			$this->userId = $auth->getIdentity()->id;
			$this->UserAccountno = $auth->getIdentity()->accountno;
		}
		
		if($this->role==102){ // if agent
			$this->agentid = $auth->getIdentity()->agentid;
		
		}
		
		if($this->role==103){ // if agent
			$this->shopid = $auth->getIdentity()->shopid;
		}
		
		
		
    }

    public function indexAction()
    {
        //fetch all payements
		if($this->role==102){
			//fetch all payments by this bulking that the current logged u user belongs 
			$payments = $this->ModelPayments->fetchPaymentsByAgentsPerBA($this->role ,$this->agentid);
			$this->view->payments = $payments;
		}
		if($this->role==103){ // if agent
			//fetch all payments made at this shop;
			$payments = $this->ModelPayments->fetchPaymentsByClerksPerShop(NULL,$this->shopid);
			$this->view->payments = $payments;
		}
		
		
		
		
    }

    public function newAction()
    {
      }

    public function historyAction()
    {
        if(null !==($this->_request->getParam('account'))){
       		$accountno = $this->_request->getParam('account');
			 
			 $this->view->accountno = $accountno;
			 
			 $this->view->payment = $this->ModelPayments->fetchSumPaymentsByPayee($accountno);
			 
			 //fetch all purchases from this farmer
			 $this->view->purchases= $this->modelPurchases->fetchSumPurchasesPerAccount($accountno);
			 
			 $exchanges = $this->ModelPayments->fetchPaymentsByPayee($accountno);
			 $this->view->exchanges = $exchanges;
			$this->view->sumtotal = $this->ModelPayments->fetchSumPaymentsPerPayee($accountno);
    	}
    }


}





