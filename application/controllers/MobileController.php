<?php

class MobileController extends Zend_Controller_Action
{

    public function init()
    {
         $this->_newForm = new Form_Purchase();
		$this->_newForm->setmethod('post');
		$this->_newForm->setAction('/purchase/new');
		$this->view->form= $this->_newForm;
		
		$this->_modelPurchases = new Model_Purchase();
		$this->_modelMembers = new Model_Members();
		$this->modelSMS = new Model_Sms();
		$this->modelRates  = new Model_Rates();
		$this->view->members = $this->_modelMembers->fetchAllData();
		$this->_redirector = $this->_helper->getHelper('Redirector');
		
		
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			$this->view->message =$message;
    	} 
		if(isset($_GET['error'])){
    		$mid=$_GET['error'];
			$this->view->mid =$mid;
			
    	}  
		
		
    }

    public function indexAction()
    {
        // action body
    }

    public function purchaseAction()
    {
        	
        $this->_helper->layout->disableLayout();
		$this->getHelper('ViewRenderer')->setNoRender();
	    $webhook_secret = 'CCMG6CWEXZ9KCQANFLK2NE9RLNA7DL2Q'; 
		
		
		 if ($_POST['secret'] !== $webhook_secret)
	    {
	        header('HTTP/1.1 403 Forbidden');
	        echo "Invalid webhook secret";
	    }
	    else 
	    {
	    	if ($_POST['event'] == 'incoming_message')
	        {
	            $message = $_POST['content'];
	            $from_number = $_POST['from_number'];
	            $phone_id = $_POST['phone_id'];
	            
	            //fetch account no of the from_number
	            $member = $this->_modelMembers->fetchByPhoneNo($from_number);
				
				if($member){
					//explode content to tell the seller Account no and quantity
		            $message = explode('#', $message);
					
					if(count($message)>1){
						$sellerAccountno = $message['0'];
						$farmer = $this->_modelMembers->fetchByAccountNo($sellerAccountno);
						if($farmer){
							$sellerAccountno = $farmer->accountno;
							$quantity = $message['1'];
			            
				            //fetch  the current rate for the shop 
				            if($member->agentcatid==102){
				            	
				            	$rate =$this->modelRates->fetchLatestAgentbuyingRate($member->agentid);
								if($rate){
								$rate = $rate->rate;
									//calculate value quantity * rate
					            	if(is_numeric($quantity)){
					            		$value = $rate * $quantity;
					            		//insert to the db
						            	
						            	$data = array();
										$data['sellerAccount'] = $sellerAccountno;
										$data['quantity'] = $quantity;
										$data['rate'] = $rate;
										$data['value'] = $value;
										$data['buyerAccount'] = $member->accountno;
						            	
						            	$res = $this->_modelPurchases->addData($data);
										
										if($res){
											//send a message to the farmer about hte receipt
											//fetch farmer phone no
											$farmerphone = $farmer->phoneno;
											if($farmerphone!==''){
												$to= $this->modelSMS->formatPhoneno($farmerphone);
												$message = 'Akili eT: '.$quantity.' Ltrs recieved at a rate of '.$rate.' /l. Thanks for using Akili eT.';
												$this->modelSMS->sendSMS($to, $message);
											}
											$this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8');
								            echo json_encode(array(
								                'messages' => array(
								                    array('content' => "Success!")
								                )
								            ));
										}
						            	
							            
					            	}else{
			            				$to= $this->modelSMS->formatPhoneno($from_number);
										$message = 'Akili eT: '.$quantity.' provided is not a valid amount.Provide a valid amount. www.akili.dev';
										$this->modelSMS->sendSMS($to, $message);
					            	}
							        
								}else{
									//send  message that the rate has not been set. Call someone
									$to= $this->modelSMS->formatPhoneno($from_number);
									$message = 'Akili eT: Rate has not been set for this agent. Call a representive for assistance. www.akili.dev';
									$this->modelSMS->sendSMS($to, $message);
								}
								
				            }else{
				            	//send message that the phone is not registered as an agent
				            	
				            	$to= $this->modelSMS->formatPhoneno($from_number);
								$message = 'Akili eT: Your phone no is not registered. Call a representive for assistance. www.akili.dev';
								$this->modelSMS->sendSMS($to, $message);
				            }
						
						
							
						}else{
							//send message that we did no recorgnize the farmer account
							
							$to= $this->modelSMS->formatPhoneno($from_number);
							$message = 'Akili eT: Invalid farmer eT Account, check the he/she is registered or call a rep for assistance. www.akili.dev';
							$this->modelSMS->sendSMS($to, $message);
						}
						
						
			            
		        	}else{
		        		//send a message about invalid message format Syntax etAccount#quanty e.g 12333#50
		        		
		        		$to= $this->modelSMS->formatPhoneno($from_number);
						$message = 'Akili eT: Invalid message format. Syntax FarmerAccountNo#quantity e.g 12333#50. www.akili.dev';
						$this->modelSMS->sendSMS($to, $message);
		        	}
				}else{
					//send message that we did not recognize the phone no
					$to= $this->modelSMS->formatPhoneno($from_number);
					$message = 'Akili eT: Invalid Akili eT Agent. Check that you are registered as an Agent. www.akili.dev';
					$this->modelSMS->sendSMS($to, $message);
	        	}
			}
		}
	    	
			
		
        
    }

    public function sellAction()
    {
        // action body
    }


}





