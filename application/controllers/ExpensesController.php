<?php

class ExpensesController extends Zend_Controller_Action
{

    public function init()
    {
	   		
       	$this->modelExpenses = new Model_Expenses();
	   	$this->_redirector = $this->_helper->getHelper('Redirector');
		$auth = Zend_Auth::getInstance();
		if (!$auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/account/login');
		}
		
		if($auth->getIdentity()->agentcatid!==''){
			$this->role = $auth->getIdentity()->agentcatid;
			$this->userId = $auth->getIdentity()->id;
			$this->UserAccountno = $auth->getIdentity()->accountno;
			
			//fetch shop id of the current logged in user
			if($this->role==102){
				$this->agentid = $auth->getIdentity()->agentid;
			}elseif($this->role==103){
				$this->shopid = $auth->getIdentity()->shopid;
			}
		}
		
		
		
		
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			$this->view->message =$message;
    	} 
		if(isset($_GET['error'])){
    		$mid=$_GET['error'];
			$this->view->mid =$mid;
			
    	} 
    }

    public function indexAction()
    {
        	
		if($this->role==102){
			//fetch all expenses for agents
			$this->view->expenses = $this->modelExpenses->fetchDataByAgentid($this->agentid);
		}elseif($this->role==103){
			//fetch all expenses for ziwa shop
			$this->view->expenses = $this->modelExpenses->fetchDataByShopid($this->shopid);
			
		}elseif($this->role==106){
			//fetch all expenses from all shops
			$this->view->expenses = $this->modelExpenses->fetchData();
		}	
			
        $form = new Form_Expense();
		$form->setAction('/expenses');
		$form->setMethod('post');
		
		if ($this->getRequest()->isPost()){
				
			if( $form->isValid($_POST)){
				$data=$form->getValues();
				
				
				
				if(count($data)>0){
					
					if($data['expensedate']==''){
						unset($data['expensedate']);
					}
					
					if($this->role==102){
						//record the agentid
						
						$data['agentid'] = $this->agentid;
						
					}elseif($this->role==103){
						//record the shopid of the current logged in user
						$data['shopid'] = $this->shopid;
						
					}
					
					
						
					$result=$this->modelExpenses->addData($data);	
					if($result){
						$this->_redirector->gotoUrl('/expenses?message=success! Expense recorded successfully');
					}else{
						$this->_redirector->gotoUrl('/expenses?error=1&message=Error ocurred while inserting the data');
					}
				}else{
					$this->_redirector->gotoUrl('/expenses?error=1&message=Error ocurred while inserting the data');
				}
			}
		}
		
		
		
		$this->view->form=$form;
		
    }

    public function newAction()
    {
        // action body
    }


}



