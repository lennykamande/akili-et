<?php

class AccountController extends Zend_Controller_Action
{

    protected $modelusers = null;

    public function init()
    {
        $layout = $this->_helper->layout()->setLayout('account');
		$this->view->headTitle('Account');
		$this->modelusers = new Model_Members();
		
		
		$this->modelMembers = new Model_Members();
		$this->modelPurchases = new Model_Purchase();
		$this->modelNyumbaKumi = new Model_Nyumbakumi();
		$this->modelAgents = new Model_Agents();
		$this->modelShops= new Model_Shops();
		$this->modelInputShops = new Model_Inputshops();
		$this->modelPayments = new Model_Payments();
		$this->modelSales = new Model_Sales();
		$this->modelSMS = new Model_Sms();
		$this->modelMailing = new Model_Mailing();
		
		
		
		
		$this->_redirector = $this->_helper->getHelper('Redirector');
		
		
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			if($auth->getIdentity()->agentcatid!==''){
				$this->role = $auth->getIdentity()->agentcatid;
				$this->userId = $auth->getIdentity()->id;
				$this->UserAccountno = $auth->getIdentity()->accountno;
				
				//fetch shop id of the current logged in user
				if($this->role==102){
					$this->agentid = $auth->getIdentity()->agentid;
				}elseif($this->role==103){
					$this->shopid = $auth->getIdentity()->shopid;
				}
			}
		}
		
		
		
		
		
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			$this->view->message =$message;
    	} 
		if(isset($_GET['error'])){
    		$mid=$_GET['error'];
			$this->view->mid =$mid;
			
    	} 
		
		
		
    }

    public function indexAction()
    {
        $auth = Zend_Auth::getInstance();
		if ($auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/hub');
		}else{
			$this->_redirector->gotoUrl('/account/login');
    	
    }
	}
    public function loginAction()
    {
    	$auth = Zend_Auth::getInstance();
		if ($auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/hub');
		}else{
	        $request = $this->getRequest();
			   //validate user
			   if ($this->_request->isPost()){
				    $data = $request->getPost();
					//set up the auth adapter
					// get the default db adapter
					$db = Zend_Db_Table::getDefaultAdapter();
					//create the auth adapter
					$authAdapter = new Zend_Auth_Adapter_DbTable($db, 'users','email', 'password');
					
					//set the username and password
					$authAdapter->setIdentity($data['email']);
					$email=strtolower($data['email']);
					
					$authAdapter->setCredential(md5($data['password']));
					
					//authenticate
					$result = $authAdapter->authenticate();
					
					if ($result->isValid()){
							
						//check of pending
						$canlogin = $this->modelusers->checkCanLogin($email);
						if(!$canlogin){
							$this->view->message = "Account not activated. Contact info@everythingwedding.co.ke for activation queries";
						}else{
							
						// store the username, first and last names of the user
						$dateObject = new Zend_Date();
						$date2 = $dateObject->get(Zend_Date::TIMESTAMP);
							 
						$auth = Zend_Auth::getInstance();
						$storage = $auth->getStorage();
						$storage->write($authAdapter->getResultRowObject(
						
							array('id','fname','lname','agentcatid','parentagentid','nyumbakumi','shopid','inputshopid','agentid','accountno','guid')));
						
						//redirect to dashboard
		           		$this->_redirector->gotoUrl('/hub'); 
						}
						
					} else {
							$this->view->message = "Sorry, your username or password was incorrect";
					}
					
				}
    	}
    }

    public function registerAction()
    {
    	$auth = Zend_Auth::getInstance();
		if ($auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/hub');
		}else{
	        $request = $this->getRequest();
		   //validate user
		   if ($this->_request->isPost()){
			    $data = $request->getPost();
			   if(count($data)>0){
			   		$data['password'] = md5($data['password']);
				   $data['loginstatus'] = 0;
				   $result = $this->modelusers->addData($data);
				   if($result){
				   		$this->view->message = "Success";
				   }else{
				   		$this->view->message = "Error";
				   }
			   }
		   }
		
    
    
    }
	}

    public function logoutAction()
    {
        // action body
        $authAdapter = Zend_Auth::getInstance();
		$authAdapter->clearIdentity();
		$this->view->headTitle('Logout');
		
		//redirect to login
   		$this->_redirector->gotoUrl('/account/login');
    }

    public function forgotPasswordAction()
    {
        $auth = Zend_Auth::getInstance();
		if ($auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/hub');
		}else{
			//put here the forgot password thingy
		
    
    
    }
	}
    public function activateAction()
    {
      $auth = Zend_Auth::getInstance();
		if ($auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/hub');
		}else{
			//put hre the activation thingy
    }
	}
    public function profileAction()
    {
        $layout = $this->_helper->layout()->setLayout('layout');
		
		if($this->UserAccountno){
       		$accountno = $this->UserAccountno;
		   $member = $this->modelMembers->fetchByAccountNo($accountno);
		   
		   $this->view->member = $member;
		   
		   if($member->agentcatid==101){
		   	 //if looking for farmer then fetch from db where seller = current accountmember clicked
		   	 	$this->view->sales = $this->modelPurchases->fetchPurchasesBySellerAccount($accountno);
		   }elseif($member->agentcatid==105){
		   		$this->view->sales = $this->modelSales->fetchSalesToAccountno($accountno);
		   }else{
		   		$this->view->sales = $this->modelSales->fetchSalesByAccountno($accountno);
		   }
		   //fetch deliveries
		   $this->view->purchases = $this->modelPurchases->fetchPurchasesByAccountno($accountno);
		   
		   
	   		//fetch paymentss made to him
	   		if($member->agentcatid==101){
	   			$this->view->payments = $this->modelPayments->fetchPaymentsByPayee($accountno);
	   		}elseif($member->agentcatid==102){
	   			$this->view->payments = $this->modelPayments->fetchPaymentsByAgentsPerBA(null,$member->agentid);
	   		}elseif($member->agentcatid==103){
	   			$this->view->payments = $this->modelPayments->fetchPaymentsByClerksPerShop($member->shopid);
	   		}elseif($member->agentcatid==104){
	   			$this->view->payments = $this->modelPayments->fetchExchangesPerpayer($accountno);
	   		}else{
	   			$this->view->payments = $this->modelPayments->fetchData();
	   		}
			
			
			if( $member->agentcatid==104){
	   			$this->view->exchanges = $this->modelPayments->fetchExchangesPerpayer($accountno);
	   		}elseif($member->agentcatid==101){
	   			$this->view->exchanges = $this->modelPayments->fetchExchangesByPayee($accountno);
				
	   		}
			
			
			
			//fetch changes
	   		
       	}
    }

    public function settingsAction()
    {
        $layout = $this->_helper->layout()->setLayout('layout');
		

 
    }

    public function createPasswordAction()
    {
      	$auth = Zend_Auth::getInstance();
		if ($auth->hasIdentity()) {
           	 //logout if already logged in
			$auth->clearIdentity();
		}
		
		if(null !==($this->_request->getParam('guid'))){
   		$guid = $this->_request->getParam('guid');	
		
		
		$formCreatepass = new Form_Createpassword();
		$formCreatepass->setMethod('post');
		$formCreatepass->setAction('/member/create-password/'.$guid);
		
		$request = $this->getRequest();
		
		 //validate user
	   if ($this->_request->isPost()){
	   		$data = $request->getPost();
		   	
			if($data['password']== $data['cpassword']){
				//update the db
				$member = $this->modelusers->fetchByActivationkey($guid);
				
				$data['password'] = md5($data['password']);
				$data['loginstatus'] = 1;
				
				if($member){
					$result = $this->modelusers->updateData($member->accountno, $data);
					if($result){
						$this->_redirector->gotoUrl('/account/login');
					}else{
						$this->view->message = 'No Matching account found';
					}
				}else{
						$this->view->message = 'No Matching account found';
				}
				
			}else{
				$this->view->message = 'Passwords do not match';
			}
		   
	   }
		   
		   
		
		$this->view->form = $formCreatepass;
		//display create password form
		
		
	}
		
		
    }


}

















