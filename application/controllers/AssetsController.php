<?php

class AssetsController extends Zend_Controller_Action
{

    public function init()
    {
       $this->modelAssets  = new Model_Assets();
		
	   	$this->_redirector = $this->_helper->getHelper('Redirector');
		$auth = Zend_Auth::getInstance();
		if (!$auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/account/login');
		}
		
		if($auth->getIdentity()->agentcatid!==''){
			$this->role = $auth->getIdentity()->agentcatid;
			$this->userId = $auth->getIdentity()->id;
			$this->UserAccountno = $auth->getIdentity()->accountno;
			
			//fetch shop id of the current logged in user
			if($this->role==102){
				$this->agentid = $auth->getIdentity()->agentid;
			}elseif($this->role==103){
				$this->shopid = $auth->getIdentity()->shopid;
			}
		}
		
		
		
		
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			$this->view->message =$message;
    	} 
		if(isset($_GET['error'])){
    		$mid=$_GET['error'];
			$this->view->mid =$mid;
			
    	} 
		
    }

    public function indexAction()
    {
		
		 if($this->role==102){
			//fetch all assets  for agents
			$this->view->assets = $this->modelAssets->fetchDataByAgentid($this->agentid);
		}elseif($this->role==103){
			//fetch all assets for ziwa shop
			$this->view->assets = $this->modelAssets->fetchDataByShopid($this->shopid);
			
		}elseif($this->role==106){
			//fetch all assets from all shops and agents
			$this->view->assets = $this->modelAssets->fetchData();
		}	
		
		$form = new Form_Asset();
		$form->setAction('/assets');
		$form->setMethod('post');
		
		if ($this->getRequest()->isPost()){
				
			if( $form->isValid($_POST)){
				$data=$form->getValues();
				
				
				
				if(count($data)>0){
					
					
					if(empty($data['value'])){
						unset($data['value']);
					}
					
					if($this->role==102){
						//record the agentid
						
						$data['agentid'] = $this->agentid;
						
					}elseif($this->role==103){
						//record the shopid of the current logged in user
						$data['shopid'] = $this->shopid;
						
					}
					
						$res = $this->modelAssets->addData($data);
						
						if($res){
							$this->_redirector->gotoUrl('/assets?message=success! Asset recorded successfully');
						}else{
							$this->_redirector->gotoUrl('/assets?error=1&message=Error ocurred while inserting the data');
						}
				}else{
					$this->_redirector->gotoUrl('/assets?error=1&message=Error ocurred while inserting the data');
				}
			}
		}
		
		$this->view->form = $form;
		
		
		
    }

    public function newAction()
    {
       
    }


}



