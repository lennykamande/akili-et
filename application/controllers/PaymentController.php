<?php

class PaymentController extends Zend_Controller_Action
{

    protected $ModelPayments = null;

    protected $modelPurchases = null;

    public function init()
    {
        $this->ModelPayments = new Model_Payments();
		$this->modelPurchases = new Model_Purchase();
		
		
		$auth = Zend_Auth::getInstance();
		if (!$auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/account/login');
		}
		
		if($auth->getIdentity()->agentcatid!==''){
			$this->role = $auth->getIdentity()->agentcatid;
			$this->userId = $auth->getIdentity()->id;
			$this->UserAccountno = $auth->getIdentity()->accountno;
		}
		
		if($this->role==102){ // if agent
			$this->agentid = $auth->getIdentity()->agentid;
		
		}
		
		if($this->role==103){ // if agent
			$this->shopid = $auth->getIdentity()->shopid;
		}
		
		
		
    }

    public function indexAction()
    {
        //fetch all payements
		if($this->role==102){
			//fetch all payments by this bulking that the current logged u user belongs 
			$payments = $this->ModelPayments->fetchPaymentsByAgentsPerBA($this->role ,$this->agentid);
			$this->view->payments = $payments;
		}
		if($this->role==103){ // if agent
			//fetch all payments made at this shop;
			$payments = $this->ModelPayments->fetchPaymentsByClerksPerShop(NULL,$this->shopid);
			$this->view->payments = $payments;
		}
		
		
		
		
    }

    public function newAction()
    {
       //fetch all payments due
       if($this->role==102){ // if agent
	   		$this->view->accountsdue = $this->modelPurchases->fetchSumPurchasesByAgentsPerBA($this->role,$this->agentid);
	   		
		}
	   
	   if($this->role==103){ // if clerk
	   		$this->view->accountsdue = $this->modelPurchases->fetchSumPurchasesByClerksPerShop($this->shopid);
		}
       
	   
	   if($this->_request->isPost()){
			$data = $this->_request->getParams();
		  	$data['payer'] = $this->UserAccountno;
			$data['payerorg'] = 1;
			$data['description'] = 'Cash payment';
			$this->view->payee = $data['payee'];
			
			if($data['value']){
				$result = $this->ModelPayments->addData($data);
				
				//send sms to payee
				$modelSMS = new Model_Sms();
				$modelMembers = new Model_Members();
				//fetch payee phone no
				$payeephone = $modelMembers->fetchByAccountNo($data['payee']);
				
				if($payeephone->phoneno!==''){
						
					$message='AKILI eT: '.CURRENCY.' '.$data['value'].' has been processed, kindly await MPESA message';
					
					$smsresult  = $modelSMS->sendSMS($payeephone->phoneno, $message);
					
				}
				
				if($result){
					$this->view->message= 'Success!';
				}else{
					$this->view->message= 'Error!';
				}
			}
			else{
				$this->view->message= 'Enter value!';
			}
			
		   	
	   }
	   
	  
       
    }

    public function historyAction()
    {
        if(null !==($this->_request->getParam('account'))){
       		$accountno = $this->_request->getParam('account');
			 
			 $this->view->accountno = $accountno;
			 
			 $this->view->payment = $this->ModelPayments->fetchSumPaymentsByPayee($accountno);
			 
			 //fetch all purchases from this farmer
			 $this->view->purchases= $this->modelPurchases->fetchSumPurchasesPerAccount($accountno);
			 
			 $exchanges = $this->ModelPayments->fetchPaymentsByPayee($accountno);
			 $this->view->exchanges = $exchanges;
			$this->view->sumtotal = $this->ModelPayments->fetchSumPaymentsPerPayee($accountno);
    	}
    }


}





