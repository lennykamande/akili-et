<?php

class CustomersController extends Zend_Controller_Action
{
	protected $modelMembers;
	protected $modelPurchases;
	protected $modelSales;
 	
	protected $formCustomer;
    public function init()
    {
        $this->modelMembers = new Model_Members();
		$this->modelPurchases = new Model_Purchase();
		$this->modelSales = new Model_Sales();
		
		$this->formCustomer = new Form_Customer();
		
		
    }

    public function indexAction()
    {
        // action body
    }

    public function editAction()
    {
        // action body
    }

    public function deleteAction()
    {
        // action body
    }

    public function detailsAction()
    {
       if(null !==($this->_request->getParam('guid'))){
       		$guid = $this->_request->getParam('guid');
		   
		   //fetch details
		   $member  = $this->modelMembers->fetchByGuid($guid);
		   $this->view->member= $member;
		   
		   
		   //fetch all purchases by this custome
		   $this->view->purchases = $this->modelSales->fetchSalesByCustomerAccountno($member->accountno);
		   
		   
	   }
    }


}







