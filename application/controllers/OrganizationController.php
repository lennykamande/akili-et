<?php

class OrganizationController extends Zend_Controller_Action
{

    protected $formAgents = null;

    protected $modelOrganization = null;

    public function init()
    {
        $this->modelOrganization = new Model_Organization();	
        $this->modelAgents = new Model_Agents();
		$this->modelPayments = new Model_Payments(); 
		$this->modelMembers = new Model_Members();
		$this->modelPurchases = new Model_Purchase();
		$this->modelSMS = new Model_Sms();
		
		$this->modelSales = new Model_Sales();
		$this->view->members = $this->modelMembers->fetchFarmers();
		
		$this->formAgents = new Form_Bulkingagent();
		$this->_redirector = $this->_helper->getHelper('Redirector');
		
		
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			$this->view->message =$message;
    	} 
		if(isset($_GET['error'])){
    		$mid=$_GET['error'];
			$this->view->mid =$mid;
			
    	}  
		
		
		$auth = Zend_Auth::getInstance();
		if (!$auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/account/login');
		}
		
		
	   if($auth->getIdentity()->agentcatid!==''){
			$this->role = $auth->getIdentity()->agentcatid;
		   $this->exchangeshopid = $auth->getIdentity()->inputshopid;
			$this->userId = $auth->getIdentity()->id;
			$this->UserAccountno = $auth->getIdentity()->accountno;
		}
	   
	   
    }

    public function indexAction()
    {
        $exchanges = $this->modelPayments->fetchSumExchangesByPayee();
		// $exchanges = $exchanges->toArray();
		// print_r($exchanges);exit;
		if($exchanges){
			$this->view->exchanges = $exchanges;
		
    }
	}

    public function makeAction()
    {
        $formExchange = new Form_Exchange();
		
		$formExchange->setAction('/exchange/make');
		$formExchange->setMethod('post');
		
		 if ($this->getRequest()->isPost()){
			if( $formExchange->isValid($_POST)){
				$data=$formExchange->getValues();
				
				$account=trim($formExchange->getValue('payee'));
				$accountArray = explode('#', $account);
				
				print_r($accountArray);
				$data['payee'] = $accountArray['0'];
				
				$farmerAccount = $this->modelMembers->fetchByAccountNo($data['payee']);
				
				if($farmerAccount){
					$data['payer'] = $this->UserAccountno;
					$data['paymenttype'] = 'exchange';
				
					//insert data to db
					$id= $this->modelPayments->addData($data);
					
					if($id){
						//send a message to farmer
						$message='AKILI eT: Product or Service woth . Thanks for using Akili eT.';
						$this->modelSMS->sendSMS($farmerAccount->phoneno, $message);
						
						$this->_redirector->gotoUrl('/exchange/?message=Success!');
					}else{
						$this->_redirector->gotoUrl('/sale/make?error=1&message=Error occured. Contact support');
					}
				}
				
			 
			}
		 }
				
				
				
		$this->view->form = $formExchange;
    }





}