<?php

class ShopsController extends Zend_Controller_Action
{
	protected $modelShops;
	protected $modelMembers;
	protected $modelPurchases = null;
	protected $modelSales = null;
	
	
	protected $formShops;
    public function init()
    {
        $this->modelShops = new Model_Shops();
		$this->modelMembers= new Model_Members();
		$this->modelPurchases = new Model_Purchase();
		$this->modelSales = new Model_Sales();
		
		$this->formShops = new Form_Shops();
		$this->_redirector = $this->_helper->getHelper('Redirector');
    }

    public function indexAction()
    {
        
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			if($message=='success'){
				$this->view->message = 'Success!';
			}elseif($message=='error'){
				$this->view->message = 'Error occured while saving data, contact support!';
				$this->view->error = 1;
			}
    	}  	
		
			
        $shops = $this->modelShops->fetchData();
		
		$this->view->shops =$shops;
			
        $form = $this->formShops;
		$form->setAction('/shops');
		$form->setMethod('post');
		
		if ($this->getRequest()->isPost()){
				
			if( $form->isValid($_POST)){
				$data=$form->getValues();
				if(count($data)>0){
					$result=$this->modelShops->addData($data);	
					if($result){
						$this->_redirector->gotoUrl('/shops?message=success');
					}else{
						$this->_redirector->gotoUrl('/shops?message=error');
					}
				}else{
					$this->_redirector->gotoUrl('/shops?message=error');
				}
			}
		}
		
		
		
		
		$this->view->form = $form;
    }

    public function editAction()
    {
	    if(null !==($this->_request->getParam('guid'))){
       		$guid = $this->_request->getParam('guid');
			
			if(isset($_GET['message'])){
    		$message=$_GET['message'];
			 
			if($message=='success'){
				$this->view->message = 'Success!';
			}elseif($message=='error'){
				$this->view->message = 'Error occured while saving data, contact support!';
				$this->view->error = 1;
			}
    	}  	
		$shop = $this->modelShops->fetchByGuid($guid);	
        
        $form = $this->formShops;
		$form->setAction('/shops/edit/'.$guid);
		$form->setMethod('post');
		$form->populate($shop->toArray());
		if ($this->getRequest()->isPost()){
				
			if( $form->isValid($_POST)){
				$data=$form->getValues();
				if(count($data)>0){
					$result=$this->modelShops->updateData($shop->id, $data);
					if($result){
						$this->_redirector->gotoUrl('/shops/edit/'.$guid.'?message=success');
					}else{
						$this->_redirector->gotoUrl('/shops/edit/'.$guid.'?message=error');
					}
				}else{
					$this->_redirector->gotoUrl('/shops/edit/'.$guid.'?message=error');
				}
			}
		}
			
        $this->view->form  = $form;
		}
    }

    public function deleteAction()
    {
       if(null !==($this->_request->getParam('guid'))){
       		$guid = $this->_request->getParam('guid');
			
			if(isset($_GET['message'])){
    		$message=$_GET['message'];
			 
			if($message=='success'){
				$this->view->message = 'Success!';
			}elseif($message=='error'){
				$this->view->message = 'Error occured while saving data, contact support!';
				$this->view->error = 1;
			}
    	} 
			$result = $this->modelShops->deletebyguid($guid);
			if($result){
				$this->_redirector->gotoUrl('/shops?message=success');
			}else{
				$this->_redirector->gotoUrl('/shops?message=error');
			}
			
	   }
	  
    }

    public function newAction()
    {
        if(isset($_GET['message'])){
    		$message=$_GET['message'];
			 
			if($message=='success'){
				$this->view->message = 'Success!';
			}elseif($message=='error'){
				$this->view->message = 'Error occured while saving data, contact support!';
				$this->view->error = 1;
			}
    	}  	
			
        $form = $this->formShops;
		$form->setAction('/shops/new');
		$form->setMethod('post');
		
		if ($this->getRequest()->isPost()){
				
			if( $form->isValid($_POST)){
				$data=$form->getValues();
				if(count($data)>0){
					$result=$this->modelShops->addData($data);	
					if($result){
						$this->_redirector->gotoUrl('/shops/new?message=success');
					}else{
						$this->_redirector->gotoUrl('/shops/new?message=error');
					}
				}else{
					$this->_redirector->gotoUrl('/shops/new?message=error');
				}
			}
		}
			
        $this->view->form  = $form;
    }

    public function detailsAction()
    {
       if(null !==($this->_request->getParam('guid'))){
       		$guid = $this->_request->getParam('guid');
		   
		   $shop = $this->modelShops->fetchByGuid($guid);
		   $this->view->member = $shop;
		   
		   
		   //fetch clerks
		   $clerks = $this->modelMembers->fetchShopAttendantsByShop($shop->id);
		   $this->view->clerks = $clerks;
		   
		   //fetch purchases
		   $this->view->purchases = $this->modelPurchases->fetchPurchasesByClerksPerShop($shop->id);
		   
		   //fetch sales
		   $this->view->sales = $this->modelSales->fetchSalesByClerksPerShop($shop->id);
		    
		   //fetch all expenses
		   
		   //fetch rates
		   
		   
		}
    }


}









