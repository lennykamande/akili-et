<?php

class PurchaseController extends Zend_Controller_Action
{

    protected $_newForm = null;

    protected $_modelPurchases = null;

    protected $_modelMembers = null;

    protected $modelSMS = null;

    protected $role = null;

    protected $userId = null;

    protected $UserAccountno = null;

    protected $modelRates = null;

    protected $buyingRate = null;

    protected $sellingRate = null;

    public function init()
    {
        $this->_newForm = new Form_Purchase();
		$this->_newForm->setmethod('post');
		$this->_newForm->setAction('/purchase/new');
		$this->view->form= $this->_newForm;
		
		$this->_modelPurchases = new Model_Purchase();
		$this->_modelMembers = new Model_Members();
		$this->modelSMS = new Model_Sms();
		$this->modelRates  = new Model_Rates();
		$this->view->members = $this->_modelMembers->fetchAllData();
		$this->_redirector = $this->_helper->getHelper('Redirector');
		
		$auth = Zend_Auth::getInstance();
		if (!$auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/account/login');
		}
		
		if($auth->getIdentity()->agentcatid!==''){
			$this->role = $auth->getIdentity()->agentcatid;
			$this->userId = $auth->getIdentity()->id;
			$this->UserAccountno = $auth->getIdentity()->accountno;
		}
		if($this->role==102){ // if agent
			$this->agentid = $auth->getIdentity()->agentid;
		
			//get the latest buying rate for the person logged in;
			if($this->agentid){
				$buyingRate = $this->modelRates->fetchLatestAgentbuyingRate($this->agentid);
				if($buyingRate){
					$this->buyingRate = $buyingRate->rate;
				}else{
					$this->buyingRate=null;
					
				}
				
			}
			
		}
		if($this->role==103){  //if clerk
			$this->shopid = $auth->getIdentity()->shopid;
		
			//get the latest buying rate for the person logged in;
			if($this->shopid){
				$buyingRate = $this->modelRates->fetchLatestShopbuyingRate($this->shopid);
				
				$this->buyingRate = $buyingRate->rate;
			}
			
		}
		
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			$this->view->message =$message;
    	} 
		if(isset($_GET['error'])){
    		$mid=$_GET['error'];
			$this->view->mid =$mid;
			
    	}  
		
		
    }

    public function indexAction()
    {
		$role = $this->role;
		$userId = $this->userId;
		if(!$this->buyingRate){
			$this->view->message= 'No rate has been yet for this agent, please set the rate before purchasing any produce';
			$this->view->error= 1;
		}
		//agents
		//fetch all purchases for the bulking agent that the current logged in agent belongs
		if($role==102){
			$agent = $this->_modelMembers->fetchById($userId);
			
			if($agent->agentid!==''){
				$agentorg = $agent->agentid;
				//fetch all agents in the purchases tables
				
				$purchases = $this->_modelPurchases->fetchPurchasesByAgentsPerBA($role, $agentorg);
				$purch = $purchases->toArray();
				
				if($purchases){
					$this->view->purchases = $purchases;
				}else{
					$this->view->message = "No purchase has been made yet";
					$this->view->error = 1;
				}
			}else{
				$this->view->message = "You have not been assigned an organization, please contact support";
				$this->view->error = 1;
			}
			
		}
		
		//ziwa clerks
		//fetch all purchases for the ziwa shop that the current logged in clerk belongs
		if($role==103){
			$clerk = $this->_modelMembers->fetchById($userId);
			if($clerk->shopid!==''){
				$clerkorg = $clerk->shopid;
				//fetch all agents in the purchases tables
				$purchases = $this->_modelPurchases->fetchPurchasesByClerksPerShop($clerkorg);
				if($purchases){
					$this->view->purchases = $purchases;
				}else{
					$this->view->message = "No purchase has been made yet";
					$this->view->error = 1;
				}
			}else{
				$this->view->message = "You have not been assigned an organization, please contact support";
				$this->view->error = 1;
			}
			
		}
		
		
		//admin
		//fetch all purchases from all agents and clerks (to separate agents and clerks later using tabs: say tab 1 is purchases for agents and tab 2 purchases for clerks)
		if($role==106){
			$this->view->purchases = $this->_modelPurchases->fetchData();
		}
        
    }

    public function newAction()
    {
    	if(isset($_GET['message'])){
    		$message=$_GET['message'];
			 
			if($message=='success'){
				$this->view->message = 'Success!';
			}elseif($message=='error'){
				$this->view->message = 'Error occured while saving data!';
				$this->view->error = 1;
			}
    	}  
						
		$data = array();
        
		 if ($this->getRequest()->isPost()){
			if( $this->_newForm->isValid($_POST)){
				$accountid=trim($this->_newForm->getValue('buyerAccount'));
				//get the id alone
				$accountidarray=explode('#', $accountid);
				$data['sellerAccount'] = trim($accountidarray['0']);
				
				$data['quantity']=trim($this->_newForm->getValue('quantity'));
				if($this->buyingRate){
					$data['rate'] = $this->buyingRate;
				}
				if($data['rate']){
					$data['value'] = $this->_modelPurchases->calculateValue($data['rate'], $data['quantity']);
				}else{
					$this->_redirector->gotoUrl('/purchase/new?error=1&message=Please set a rate, none has been set! You can then try again');
				}
				$data['buyerAccount'] = $this->UserAccountno;
				if($data['value']>0){
				 		
				 	//check if account no exists	
				 	if($this->_modelMembers->checkIfExists($data['sellerAccount'])){
				 			
				 		try{
						$result=$this->_modelPurchases->addData($data);
						//fetch phone number
						$phone=$this->_modelMembers->fetchByAccountNo($data['sellerAccount']);
						
						//send notification sms to the buyer
						if($phone->phoneno!==''){
							$message='AKILI eT: '.$data['quantity'].' litres of milk received at '.$data['rate'].' per litre. Thanks for using Akili eT.';
							$res=$this-> modelSMS->sendSMS($phone->phoneno, $message);
						}
						
						
						$this->_redirector->gotoUrl('/purchase/new?message=Success!');
						
						$this->view->error = 0;
						}
						catch (Exception $e){
							$this->_redirector->gotoUrl('/purchase/new?error=1&message=Error occured saving data to the server, please consult IT Support');
						}
				 	}else{
				 		
						//if only name is provided
					//break the spaced name
					$accountArray = explode(' ', $accountid);
					
					if(isset($accountArray['0'])){
						$member['fname'] =  $accountArray['0'];
					}
					if(isset($accountArray['1'])){
						$member['mname'] =  $accountArray['1'];
					}
					if(isset($accountArray['2'])){
						$member['lname'] =  $accountArray['2'];
					}

					//insert as a new record and fetch the account number of the record inserted
					$member['accountno'] = $this->_modelMembers->generateAndValidateAccountNo();
					$member['loginstatus'] =  0;
					$member['orgid'] =  1;
					$member['agentcatid'] = 101;
					if($this->role==102){
						$member['parentagentid'] = $this->agentid;
					}
					
					$member['active'] = 0;
					
					//insert data to db
					$result1 = $this->_modelMembers->addData($member);
					
					//fetch account no inserted;
					$product = $this->_modelMembers->fetchById($result1);
					
					$data['sellerAccount'] = $product->accountno;
					if($data['sellerAccount']){
						$result=$this->_modelPurchases->addData($data);
					
						if($result){
							$this->_redirector->gotoUrl('/purchase/new?message=Success!');
						}else{
							//check if account no exists
							$this->_redirector->gotoUrl('/purchase/new?error=1&message=We did not recognize the account no, kindly countercheck');
							$this->view->error = 1;
						}
					}
				 	
				 	}
				 	
				 }else{
					$this->_redirector->gotoUrl('/purchase/new?error=1&message=Cannot record because there was no quantity received');
					$this->view->error = 1;
				 }
			}
		 }
		 $this->view->form= $this->_newForm;
    }

    public function purchaseimportAction()
    {
    	define("UPLOAD_DIR", APPLICATION_PATH.'/../data/');
		
		$name = $this->uploadfile();
		if($name){
			//import data from array
       		$exceldata = $this->getDataFromExcel($name);
			
			$exceldata = array_slice($exceldata, 1);
		
			$exceldataLength = count($exceldata);
			$finalresult = array();
			
			
			
			$datalength = count($exceldata['0']['0']);
			
			// print_r('<pre>');
			// print_r($exceldata);
			// print_r('</pre>');
			
			$errorArray=array();
			
			//
			
			for ($x=0; $x < $exceldataLength ; $x++) {
				//for ($i=0; $i < $datalength ; $i++) { 
					$dateunformatted= $exceldata[$x][0][0];
					//format date from excel
					$dateTimestamp = ($dateunformatted - 25569) * 86400;
					$date=date("Y-m-d H:m:s", $dateTimestamp);
					
					$sellername= $exceldata[$x][0][1];
					$quantity= $exceldata[$x][0][2];
					$rate= $exceldata[$x][0][3];
					
					if($rate!=='' && $sellername!=='' && $quantity!=='' && $date!=='' ){
						
						$sellername = explode(' ', $sellername);
						//check if sellername exists in db
						if(!empty($sellername['0']) && !empty($sellername['1']) && !empty($sellername['2']) ){
							$selname = $this->_modelMembers->checkIfNameExists($sellername['0'], $sellername['1'], $sellername['2']);
						}elseif(!empty($sellername['0']) && !empty($sellername['1']) ){
							$selname = $this->_modelMembers->checkIfNameExists($sellername['0'], $sellername['1'], null);
						}elseif(!empty($sellername['0'])){
							$selname = $this->_modelMembers->checkIfNameExists($sellername['0'],null, null);
						}
						if($selname){
							$etAccount = $selname->accountno;
						}else{
							if(!empty($sellername['0'])){
								$da['fname']=$sellername['0'];
							}	
							if(!empty($sellername['1'])){
								$da['mname']=$sellername['1'];
							}	
							if(!empty($sellername['2'])){
								$da['lname']=$sellername['2'];
							}
								
							if($this->role==102){
								//then the records are from a farmer
								$da['agentcatid'] = 102;
								$da['agentid'] = $this->agentid;
							}elseif($this->role==103){
								//then the records are from an agent
								$da['agentcatid'] = 103;
								$da['shopid'] = $this->shopid;
							}
							
							$da['parentagentid'] = $this->userId;
							//add member to db
							$id = $this->_modelMembers->addData($da);
							if($id){
								$member = $this->_modelMembers->fetchById($id);
								$etAccount = $member->accountno;
								
							}
							
							
						}
						
						//if seller exists then return the et Account 
						
						//ifnot create an eT account and return the eT account no
						
						$data['sellerAccount']=$etAccount;
						
						$data['buyerAccount']=$this->UserAccountno;
						$data['quantity']=$quantity;
						$data['rate']=$rate;
						$data['value']=$quantity * $rate;
						
						$data['datepurchased'] = $date;
						
						if(!empty($data['value'])){
							//insert to db
							
							$res = $this->_modelPurchases->addData($data);
							//if error echo error
							if(!$res){
								array_push($errorArray,'Error while inserting '.$sellername.'record');
							}
						}
						/*
						 * Uncomment this to show that the phone number is already in db
						 */
						// else{
							// array_push($errorArray,'Phone number in db already: '.$phone);
						// }
						
					}else{
						array_push($errorArray,'Rate or Quantity or Seller name not set for record no: <b>'.$x.'</b>');
					}
					
					
					
					
			}
			$this->view->errorArray = $errorArray;
			$this->_redirector->gotoUrl('/purchase/new?message=Success! Records successfully imported.');
			
		}else{
			die('Error occured while uploading data');
		}
		
    }

    public function uploadfile($data = null)
    {
    	
		if (!empty($_FILES["myFile"])) {
	    	$myFile = $_FILES["myFile"];
	 
	    if ($myFile["error"] !== UPLOAD_ERR_OK) {
	        echo "<p>An error occurred.</p>";
	        exit;
	    }
	 
	    // ensure a safe filename
	    $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);
	    
	 
	    // don't overwrite an existing file
	    $i = 0;
	    $parts = pathinfo($name);
	    while (file_exists(UPLOAD_DIR . $name)) {
	        $i++;
	        $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
	    }
	 
	    // preserve file from temporary directory
	    $success = move_uploaded_file($myFile["tmp_name"],
	        UPLOAD_DIR . $name);
	    if (!$success) { 
	        echo "<p>Unable to save file.</p>";
	        exit;
	    }
	 
	    // set proper permissions on the new file
	    chmod(UPLOAD_DIR . $name, 0644);
		
		return $name;
		
    }else{
    	return null;
    }
		
    }

    public function getDataFromExcel($filename)
    {
     //  Include PHPExcel_IOFactory
	include 'PHPExcel/IOFactory.php';
	
	$inputFileName = APPLICATION_PATH.'/../data/'.$filename;
	
	//  Read your Excel workbook
	try {
	    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
	    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
	    $objPHPExcel = $objReader->load($inputFileName);
	} catch(Exception $e) {
	    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
	}
	
	//  Get worksheet dimensions
	$sheet = $objPHPExcel->getSheet(0); 
	$highestRow = $sheet->getHighestRow(); 
	$highestColumn = "J";
	$data = array();
	//  Loop through each row of the worksheet in turn
	for ($row = 1; $row <= $highestRow; $row++){ 
	    //  Read a row of data into an array
	    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
	                                    NULL,
	                                    TRUE,
	                                    FALSE);
	    //  Insert row data array into your database of choice here
	array_push($data, $rowData);
	}
	 if($data){
	 	return ($data);
	 }else{
	 	throw new Exception("Error getting data from excel", 1);
		 
	 }
	
    }

    public function smsAction()
    {
    	
    	$this->_helper->layout->disableLayout();
		$this->getHelper('ViewRenderer')->setNoRender();
	    
        if ($_POST['event'] == 'incoming_message')
        {
            $message = $_POST['content'];
            $from_number = $_POST['from_number'];
            $phone_id = $_POST['phone_id'];
            
            //fetch account no of the from_number
            $member = $this->_modelMembers->fetchByPhoneNo($from_number);
			
			if($member){
				//explode content to tell the seller Account no and quantity
	            $message = explode('#', $message);
				
				if(count($message)>1){
					$sellerAccountno = $message['0'];
					$farmer = $this->_modelMembers->fetchByAccountNo($sellerAccountno);
					if($farmer){
						$sellerAccountno = $farmer->accountno;
						$quantity = $message['1'];
		            
			            //fetch  the current rate for the shop 
			            if($member->agentcatid==102){
			            	
			            	$rate =$this->modelRates->fetchLatestAgentbuyingRate($member->agentid);
							if($rate){
							$rate = $rate->rate;
								//calculate value quantity * rate
				            	if(is_numeric($quantity)){
				            		$value = $rate * $quantity;
				            		//insert to the db
					            	
					            	$data = array();
									$data['sellerAccount'] = $sellerAccountno;
									$data['quantity'] = $quantity;
									$data['rate'] = $rate;
									$data['value'] = $value;
									$data['buyerAccount'] = $member->accountno;
					            	
					            	$res = $this->_modelPurchases->addData($data);
									
									if($res){
										//send a message to the farmer about hte receipt
										//fetch farmer phone no
										$farmerphone = $farmer->phoneno;
										if($farmerphone!==''){
											$to= $this->modelSMS->formatPhoneno($farmerphone);
											$message = 'Akili eT: '.$quantity.' Ltrs recieved at a rate of '.$rate.' /l. Thanks for using Akili eT.';
											$this->modelSMS->sendSMS($to, $message);
										}
										$this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8');
							            echo json_encode(array(
							                'messages' => array(
							                    array('content' => "W!")
							                )
							            ));
									}
					            	
						            
				            	}else{
		            				$to= $this->modelSMS->formatPhoneno($from_number);
									$message = 'Akili eT: '.$quantity.' provided is not a valid amount.Provide a valid amount. www.akili.dev';
									$this->modelSMS->sendSMS($to, $message);
				            	}
						        
							}else{
								//send  message that the rate has not been set. Call someone
								$to= $this->modelSMS->formatPhoneno($from_number);
								$message = 'Akili eT: Rate has not been set for this agent. Call a representive for assistance. www.akili.dev';
								$this->modelSMS->sendSMS($to, $message);
							}
							
			            }else{
			            	//send message that the phone is not registered as an agent
			            	
			            	$to= $this->modelSMS->formatPhoneno($from_number);
							$message = 'Akili eT: Your phone no is not registered. Call a representive for assistance. www.akili.dev';
							$this->modelSMS->sendSMS($to, $message);
			            }
					
					
						
					}else{
						//send message that we did no recorgnize the farmer account
						
						$to= $this->modelSMS->formatPhoneno($from_number);
						$message = 'Akili eT: Invalid farmer eT Account, check the he/she is registered or call a rep for assistance. www.akili.dev';
						$this->modelSMS->sendSMS($to, $message);
					}
					
					
		            
	        	}else{
	        		//send a message about invalid message format Syntax etAccount#quanty e.g 12333#50
	        		
	        		$to= $this->modelSMS->formatPhoneno($from_number);
					$message = 'Akili eT: Invalid message format. Syntax FarmerAccountNo#quantity e.g 12333#50. www.akili.dev';
					$this->modelSMS->sendSMS($to, $message);
	        	}
			}else{
				//send message that we did not recognize the phone no
				$to= $this->modelSMS->formatPhoneno($from_number);
				$message = 'Akili eT: Invalid Akili eT Agent. Check that you are registered as an Agent. www.akili.dev';
				$this->modelSMS->sendSMS($to, $message);
        	}
			}
             
        }
		


}







