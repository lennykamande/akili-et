<?php

class SaleController extends Zend_Controller_Action
{

    protected $formSale = null;

    protected $modelSales = null;

    protected $modelPurchases = null;

    protected $modelShops = null;

    protected $_modelMembers = null;

    protected $userId = null;

    protected $UserAccountno = null;

    protected $modelRates = null;

    protected $buyingRate = null;

    protected $sellingRate = null;

    protected $modelSMS = null;

    protected $role = null;

    public function init()
    {
    	$this->formSale = new Form_Sale();
		$this->formSale->setAction('/sale/new');
		$this->formSale->setMethod('post');
		$this->view->form = $this->formSale;
		
		
	   $this->modelSales = new Model_Sales();
	   $this->modelPurchases = new Model_Purchase();
	   $this->_modelMembers = new Model_Members();
	   $this->modelRates  = new Model_Rates();
	   $this->modelSMS = new Model_Sms();
	   $this->modelShops = new Model_Shops();
	  
	   
	   $this->_redirector = $this->_helper->getHelper('Redirector');
	   
	   $this->view->members = $this->_modelMembers->fetchAllData();
	  
	   $auth = Zend_Auth::getInstance();
		if (!$auth->hasIdentity()) {
           	$this->_redirector->gotoUrl('/account/login');
		}
		
		
	   if($auth->getIdentity()->agentcatid!==''){
			$this->role = $auth->getIdentity()->agentcatid;
			$this->userId = $auth->getIdentity()->id;
			$this->UserAccountno = $auth->getIdentity()->accountno;
		}
	   
	   
		if(isset($_GET['message'])){
    		$message=$_GET['message'];
			$this->view->message =$message;
    	} 
		if(isset($_GET['error'])){
    		$mid=$_GET['error'];
			$this->view->mid =$mid;
			
    	}  
		
		if($this->role==102){ // if agent
			$this->agentid = $auth->getIdentity()->agentid;
		
			//get the latest buying rate for the person logged in;
			if($this->agentid){
				$sellingRate = $this->modelRates->fetchLatestAgentSellingRate($this->agentid);
				
				if(!empty($sellingRate->rate)){
					$this->sellingRate = $sellingRate->rate;
				}else{
					$this->view->message = 'Selling rate has not been set, consider setting before making any sale';
				}
				
			}
			
		}
		if($this->role==103){  //if clerk
			$this->shopid = $auth->getIdentity()->shopid;
		
			//get the latest buying rate for the person logged in;
			if($this->shopid){
				$sellingRate = $this->modelRates->fetchLatestShopSellingRate($this->shopid);
				
				$this->sellingRate = $sellingRate->rate;
			}
			
		}
		
		
	   
    }

    public function indexAction()
    {
    	$role = $this->role;
		$userId = $this->userId;
		
		
		//fetch all sales belonging to an agent
		if($role==102){
			$agent = $this->_modelMembers->fetchById($userId);
			if($agent->agentid!==''){
				$agentorg = $agent->agentid;
				//fetch all agents in the purchases tables
				
				$sales = $this->modelSales->fetchSalesByAgentsPerBA($agentorg);
				
			if($sales){
					$this->view->sales = $sales;
				}else{
					$this->view->message = "No sale has been made yet";
					$this->view->error = 1;
				}
			}else{
				$this->view->message = "You have not been assigned an organization, please contact support";
				$this->view->error = 1;
			}
		}
		
		
		if($role==103){
			$clerk = $this->_modelMembers->fetchById($userId);
			
			if($clerk->agentid!==''){
				$clerkShop= $clerk->shopid;
				//fetch all agents in the purchases tables
				$sales = $this->modelSales->fetchSalesByClerksPerShop($clerkShop);
			
				if($sales){
					$this->view->sales = $sales;
				}else{
					$this->view->message = "No sale has been made yet";
					$this->view->error = 1;
				}
			}else{
				$this->view->message = "You have not been assigned an organization, please contact support";
				$this->view->error = 1;
			}
		}
		
		
		
		//fetch all sales belonging to a shop
		
		
    	//$this->view->sales = $this->modelSales->fetchData();
		
    }

    public function newAction()
    {
		
       if ($this->getRequest()->isPost()){
			if( $this->formSale->isValid($_POST)){
				$account=trim($this->formSale->getValue('customerAccount'));
				$accountArray = explode('#', $account);
				
				$data['customerAccount'] = $accountArray['0'];
				
				$data['quantity']=trim($this->formSale->getValue('quantity'));
				
				
				if( $this->sellingRate){
					$data['rate']= $this->sellingRate;
				}else{
					$this->_redirector->gotoUrl('/sale/new?error=1&message=Please set a rate, none has been set! You can then try again');	
				}
				$data['sellerAccount']=$this->UserAccountno;
				
				$data['value']=$this->modelPurchases->calculateValue($data['rate'], $data['quantity']);
				
				
				//check if customer exists, if not add as a new record to users table
				if(($data['customerAccount']*2)>0){
					//check if in db
					$result = $this->_modelMembers->checkIfExists($data['customerAccount']);
					
					//if account no of the customer buying milk is available
					if($result){
						//record new sale
						$result = $this->modelSales->addData($data);
						if($result){
							$message='AKILI eT: '.$data['quantity'].' litres of milk bought. Thanks for using Akili eT.';
							$member['phoneno'] = trim($this->formSale->getValue('phoneno'));
							
							$to=$member['phoneno'];
							
							
							$this->modelSMS->sendSMS($to, $message);
							$this->_redirector->gotoUrl('/sale/new?message=Successfully recorded a new sale.');
						}else{
							$this->view->message = 'Eror occured while recording the date. Contact support';
							$this->view->error = 1;
						}
						
					}else{
						$this->view->message = 'We did not recognize the account no provided';
						$this->view->error = 1;
					}
					
				}else{
					//if only name is provided
					//break the spaced name
					$accountArray = explode(' ', $account);
					
					if(isset($accountArray['0'])){
						$member['fname'] =  $accountArray['0'];
					}
					if(isset($accountArray['1'])){
						$member['mname'] =  $accountArray['1'];
					}
					if(isset($accountArray['2'])){
						$member['lname'] =  $accountArray['2'];
					}

					//insert as a new record and fetch the account number of the record inserted
					$member['accountno'] = $this->_modelMembers->generateAndValidateAccountNo();
					$member['loginstatus'] =  0;
					$member['orgid'] =  1;
					$member['agentcatid'] = 105;
					$member['shopid'] = $this->shopid;
					$member['active'] = 0;
					$member['phoneno'] = trim($this->formSale->getValue('phoneno'));
					
					//insert data to db
					$result1 = $this->_modelMembers->addData($member);
					//fetch account no inserted;
					$product = $this->_modelMembers->fetchById($result1);
					
					$data['customerAccount'] =  $product->accountno;
					//record new sale
					$result = $this->modelSales->addData($data);
					
					if($result){
						$this->view->message = 'Successfly recorded a new sale.';
					}else{
						$this->view->message = 'Eror occured while recording the date. Contact support';
						$this->view->error = 1;
					}
				
				}
				
			}
	   
    
    }
    }
    public function testAction()
    {
        // action body
    }

    public function salesimportAction()
    {
        define("UPLOAD_DIR", APPLICATION_PATH.'/../data/');
		
		$name = $this->uploadfile();
		if($name){
			//import data from array
       		$exceldata = $this->getDataFromExcel($name);
			
			$exceldata = array_slice($exceldata, 1);
		
			$exceldataLength = count($exceldata);
			$finalresult = array();
			
			
			
			$datalength = count($exceldata['0']['0']);
			
			// print_r('<pre>');
			// print_r($exceldata);
			// print_r('</pre>');
			
			$errorArray=array();
			
			//
			
			for ($x=0; $x < $exceldataLength ; $x++) {
				//for ($i=0; $i < $datalength ; $i++) { 
					$dateunformatted= $exceldata[$x][0][0];
					
					//format date from excel
					$dateTimestamp = ($dateunformatted - 25569) * 86400;
					$date=date("Y-m-d H:m:s", $dateTimestamp);
					
					$sellername= $exceldata[$x][0][1];
					$quantity= $exceldata[$x][0][2];
					$rate= $exceldata[$x][0][3];
					
					if($rate!=='' && $sellername!=='' && $quantity!=='' && $date!=='' ){
						
						$sellername = explode(' ', $sellername);
						//check if sellername exists in db
						if(!empty($sellername['0']) && !empty($sellername['1']) && !empty($sellername['2']) ){
							$selname = $this->_modelMembers->checkIfNameExists($sellername['0'], $sellername['1'], $sellername['2']);
						}elseif(!empty($sellername['0']) && !empty($sellername['1']) ){
							$selname = $this->_modelMembers->checkIfNameExists($sellername['0'], $sellername['1'], null);
						}elseif(!empty($sellername['0'])){
							$selname = $this->_modelMembers->checkIfNameExists($sellername['0'],null, null);
						}
						if($selname){
							$etAccount = $selname->accountno;
						}else{
							if(!empty($sellername['0'])){
								$da['fname']=$sellername['0'];
							}	
							if(!empty($sellername['1'])){
								$da['mname']=$sellername['1'];
							}	
							if(!empty($sellername['2'])){
								$da['lname']=$sellername['2'];
							}
								
							if($this->role==102){
								//then the records are to an agent
								$da['agentcatid'] = 103;
							}elseif($this->role==103){
								//then the records are to an end customer
								$da['agentcatid'] = 105;
							}
							
							$da['parentagentid'] = $this->userId;
							//add member to db
							$id = $this->_modelMembers->addData($da);
							if($id){
								$member = $this->_modelMembers->fetchById($id);
								$etAccount = $member->accountno;
								
							}
							
							
						}
						
						//if seller exists then return the et Account 
						
						//ifnot create an eT account and return the eT account no
						
						
						
						$data['customerAccount']=$etAccount;
						
						$data['sellerAccount']=$this->UserAccountno;
						$data['quantity']=$quantity;
						$data['rate']=$rate;
						$data['value']=$quantity * $rate;
						$data['datesold'] = $date;
						
						if(!empty($data['value'])){
							//insert to db
							
							$res = $this->modelSales->addData($data);
							
							//if error echo error
							if(!$res){
								array_push($errorArray,'Error while inserting '.$sellername.'record');
							}
						}
						/*
						 * Uncomment this to show that the phone number is already in db
						 */
						// else{
							// array_push($errorArray,'Phone number in db already: '.$phone);
						// }
						
					}else{
						array_push($errorArray,'Rate or Quantity or Seller name not set for record no: <b>'.$x.'</b>');
					}
					
				}
				$this->view->errorArray = $errorArray;
				$this->view->message = 'Success! Records successfully imported.';
				//$this->_redirector->gotoUrl('/sale/new?message=Success! Records successfully imported.');
			
		}else{
			die('Error occured while uploading data');
		}
		
    
    }
	
	public function uploadfile($data=null){
    	
		if (!empty($_FILES["myFile"])) {
	    	$myFile = $_FILES["myFile"];
	 
	    if ($myFile["error"] !== UPLOAD_ERR_OK) {
	        echo "<p>An error occurred.</p>";
	        exit;
	    }
	 
	    // ensure a safe filename
	    $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);
	    
	 
	    // don't overwrite an existing file
	    $i = 0;
	    $parts = pathinfo($name);
	    while (file_exists(UPLOAD_DIR . $name)) {
	        $i++;
	        $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
	    }
	 
	    // preserve file from temporary directory
	    $success = move_uploaded_file($myFile["tmp_name"],
	        UPLOAD_DIR . $name);
	    if (!$success) { 
	        echo "<p>Unable to save file.</p>";
	        exit;
	    }
	 
	    // set proper permissions on the new file
	    chmod(UPLOAD_DIR . $name, 0644);
		
		return $name;
		
    }else{
    	return null;
    }
		
    }
	 public function getDataFromExcel($filename)
    {
     //  Include PHPExcel_IOFactory
	include 'PHPExcel/IOFactory.php';
	
	$inputFileName = APPLICATION_PATH.'/../data/'.$filename;
	
	//  Read your Excel workbook
	try {
	    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
	    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
	    $objPHPExcel = $objReader->load($inputFileName);
	} catch(Exception $e) {
	    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
	}
	
	//  Get worksheet dimensions
	$sheet = $objPHPExcel->getSheet(0); 
	$highestRow = $sheet->getHighestRow(); 
	$highestColumn = "J";
	$data = array();
	//  Loop through each row of the worksheet in turn
	for ($row = 1; $row <= $highestRow; $row++){ 
	    //  Read a row of data into an array
	    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
	                                    NULL,
	                                    TRUE,
	                                    FALSE);
	    //  Insert row data array into your database of choice here
	array_push($data, $rowData);
	}
	 if($data){
	 	return ($data);
	 }else{
	 	throw new Exception("Error getting data from excel", 1);
		 
	 }
	
    }
}







