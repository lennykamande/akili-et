<?php
/**
 * Get getContent helper
 *
 * Call as $this->getProfile() in your layout script
 */
class Zend_View_Helper_GetAccountHolder extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function getAccountHolder($accountid)
    {
		$modelAccounts = new Model_Members();
		$holder = $modelAccounts->fetchByAccountNo($accountid);
		if($holder){
			return ucfirst($holder->fname).' '.ucfirst($holder->mname).' '.ucfirst($holder->lname);
		}else{
			return 'Unknown';
		}
		
		
		
    }
}
 