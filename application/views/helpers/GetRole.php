<?php
/**
 * Get getContent helper
 *
 * Call as $this->getProfile() in your layout script
 */
class Zend_View_Helper_GetRole extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function getRole()
    {
		
		$auth = Zend_Auth::getInstance();
		
		if ($auth->hasIdentity()) {
			if($auth->getIdentity()->agentcatid!==''){
				return $auth->getIdentity()->agentcatid;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
		
		
    }
}
 