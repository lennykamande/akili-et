<?php
/**
 * Get getContent helper
 *
 * Call as $this->getProfile() in your layout script
 */
class Zend_View_Helper_GetPaymentByPayee extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function getPaymentByPayee($accountno)
    {
		$modelPayments = new Model_Payments();
		$payment = $modelPayments->fetchSumPaymentsByPayee($accountno);
		if($payment){
			return $payment;
		}else{
			return FALSE;
		}
		
    }
}
 