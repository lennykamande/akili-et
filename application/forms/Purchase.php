<?php

class Form_Purchase extends Zend_Form
{

    public function init()
    {
        //Account No
        $account= $this->createElement('text', 'buyerAccount');
		$account->setLabel('eT Account');
		$account->setRequired(TRUE);
		$account->setAttrib('size', 30);
		$account->setAttrib('class', 'form-control');
		$account->setAttrib('placeholder', 'e.g 1233');
		$account->setAttrib('id', 'accounts');
		$this->addElement($account);
		
		 //Quantity
        $quantity= $this->createElement('text', 'quantity');
		$quantity->setLabel('Quantity');
		$quantity->setRequired(TRUE);
		$quantity->setAttrib('size', 30);
		$quantity->setAttrib('class', 'form-control');
		
		$this->addElement($quantity);
		
		//submit
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
    }


}

