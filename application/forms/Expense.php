<?php

class Form_Expense extends Zend_Form
{

    public function init()
    {
        //Account No
        $names= $this->createElement('text', 'description');
		$names->setLabel('Expense (Provide enough details)');
		$names->setRequired(TRUE);
		$names->setAttrib('size', 30);
		$names->setAttrib('class', 'form-control');
		$names->setAttrib('placeholder', 'e.g Fuel for Transport');
		$this->addElement($names);
		
		
		
		 //Quantity
        $email= $this->createElement('text', 'value');
		$email->setLabel('Value');
		$names->setRequired(TRUE);
		$email->setAttrib('size', 30);
		$email->setAttrib('class', 'form-control');
		$email->setAttrib('placeholder', 'Ksh');
		$this->addElement($email);
		
		//Quantity
        $phoneno= $this->createElement('text', 'expensedate');
		$phoneno->setLabel('Date (Leave empty if today)');
		$phoneno->setAttrib('size', 30);
		$phoneno->setAttrib('class', 'form-control');
		$phoneno->setAttrib('id', 'datepicker2');
		$phoneno->setAttrib('placeholder', '0000-00-00');
		$this->addElement($phoneno);
		
		
		//submit
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
		
    }
	
	


}

