<?php

class Form_Bulkingagent extends Zend_Form
{

    public function init()
    {
       //Account No
        $names= $this->createElement('text', 'name');
		$names->setLabel('Agent Name');
		$names->setRequired(TRUE);
		$names->setAttrib('size', 30);
		$names->setAttrib('class', 'form-control');
		$this->addElement($names);
		
		//Account No
        $guid= $this->createElement('hidden', 'guid');
		$guid->setAttrib('value', uniqid('et'));
		$this->addElement($guid);
		
		//Account No
        $location= $this->createElement('text', 'location');
		$location->setLabel('Location');
		$location->setAttrib('size', 30);
		$location->setAttrib('class', 'form-control');
		
		$this->addElement($location);
		
		 //Quantity
        $description= $this->createElement('textarea', 'description');
		$description->setLabel('Description');
		$description->setAttrib('size', 30);
		$description->setAttrib('class', 'form-control');
		$this->addElement($description);
		
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
    }


}

