<?php

class Form_Sale extends Zend_Form
{

    public function init()
    {
         //Account No
        $account= $this->createElement('text', 'customerAccount');
		$account->setLabel('Customer Name');
		$account->setRequired(TRUE);
		$account->setAttrib('size', 30);
		$account->setAttrib('class', 'form-control');
		$account->setAttrib('id', 'accounts');
		$this->addElement($account);
		
		
		
		 //Quantity
        $quantity= $this->createElement('text', 'quantity');
		$quantity->setLabel('Quantity');
		$quantity->setRequired(TRUE);
		$quantity->setAttrib('size', 30);
		$quantity->setAttrib('class', 'form-control');
		
		$this->addElement($quantity);
		
		 //Account No
        $phoneno= $this->createElement('text', 'phoneno');
		$phoneno->setLabel('Phone Number (07206061667)');
		$phoneno->setAttrib('size', 30);
		$phoneno->setAttrib('class', 'form-control');
		$this->addElement($phoneno);
		
		//submit
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
    }


}

