<?php

class Form_Editfarmer extends Zend_Form
{

    public function init()
    {
         //Account No
        $fname= $this->createElement('text', 'fname');
		$fname->setLabel('First Names');
		$fname->setRequired(TRUE);
		$fname->setAttrib('size', 30);
		$fname->setAttrib('class', 'form-control');
		$this->addElement($fname);
		
		//Account No
        $mname= $this->createElement('text', 'mname');
		$mname->setLabel('Middle Name');
		$mname->setRequired(TRUE);
		$mname->setAttrib('size', 30);
		$mname->setAttrib('class', 'form-control');
		$this->addElement($mname);
		
		//Account No
        $lname= $this->createElement('text', 'lname');
		$lname->setLabel('Other Names');
		$lname->setRequired(TRUE);
		$lname->setAttrib('size', 30);
		$lname->setAttrib('class', 'form-control');
		$this->addElement($lname);
		
		//counties
        $group= $this->createElement('select', 'parentagentid');
		$group->setLabel('Production Group');
		$group->setAttrib('class', 'form-control');
		
		$group->addMultiOption('', '(Choose one)');
		
		$modelAgents = new Model_Agents();
		$agents = $modelAgents->fetchData();
		$array = $agents ->toArray();
		if(count($array)>0){
			foreach ($agents as $agent) {
				$group->addMultiOption($agent->id, $agent->name);
			}
		}else{
			$group->addMultiOption(' ', 'No Groups available');
		}
		$this->addElement($group);
		
		 //Quantity
        $email= $this->createElement('text', 'email');
		$email->setLabel('Email (optional)');
		$email->setAttrib('size', 30);
		$email->setAttrib('class', 'form-control');
		$this->addElement($email);
		
		//Quantity
        $phoneno= $this->createElement('text', 'phoneno');
		$phoneno->setLabel('Phone No.');
		$phoneno->setAttrib('size', 30);
		$phoneno->setAttrib('class', 'form-control');
		$this->addElement($phoneno);
		
		
		//Quantity
        $idno= $this->createElement('text', 'idno');
		$idno->setLabel('National ID No.');
		$idno->setRequired(TRUE);
		$idno->setAttrib('size', 30);
		$idno->setAttrib('class', 'form-control');
		$this->addElement($idno);
		
		//counties
        $nyumbakumi= $this->createElement('select', 'nyumbakumi');
		$nyumbakumi->setLabel('Nyumba Kumi');
		$nyumbakumi->addMultiOption('', '(Choose one)');
		$nyumbakumi->setAttrib('class', 'form-control');
		
		$modelNK= new Model_Nyumbakumi();
		$nyumbakumis = $modelNK->fetchData();
		if($nyumbakumis){
			foreach ($nyumbakumis as $ny) {
				$nyumbakumi->addMultiOption($ny->id, $ny->name);
			}
		}
		$this->addElement($nyumbakumi);
		
		
		
		
		//submit
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
    }


}

