<?php

class Form_Ratebuy extends Zend_Form
{

    public function init()
    {
        //Quantity
        $rate= $this->createElement('text', 'rate');
		$rate->setLabel('Rate (Ksh)');
		$rate->setRequired(TRUE);
		$rate->setAttrib('size', 10);
		$rate->setAttrib('class', 'form-control');
		$this->addElement($rate);
		
		//submit
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
    }


}

