<?php

class Form_Shops extends Zend_Form
{

    public function init()
    {
        //Account No
        $names= $this->createElement('text', 'name');
		$names->setLabel('Ziwa Shop Name');
		$names->setRequired(TRUE);
		$names->setAttrib('size', 30);
		$names->setAttrib('class', 'form-control');
		$this->addElement($names);
		
		//Account No
        $location= $this->createElement('text', 'location');
		$location->setLabel('Location');
		$location->setAttrib('size', 30);
		$location->setAttrib('class', 'form-control');
		
		$this->addElement($location);
		
		 //Quantity
        $description= $this->createElement('textarea', 'description');
		$description->setLabel('Description');
		$description->setAttrib('size', 30);
		$description->setAttrib('cols',50);
		$description->setAttrib('rows',6);
		$description->setAttrib('class', 'form-control');
		$this->addElement($description);
		
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
    }


}

