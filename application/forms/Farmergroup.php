<?php

class Form_Farmergroup extends Zend_Form
{

    public function init()
    {
        $names= $this->createElement('text', 'name');
		$names->setLabel('Group Name');
		$names->setRequired(TRUE);
		$names->setAttrib('size', 30);
		$names->setAttrib('class', 'form-control');
		$this->addElement($names);
		
        $description= $this->createElement('textarea', 'description');
		$description->setLabel('Description');
		$description->setRequired(false);
		$description->setAttrib('class', 'form-control');
		$this->addElement($description);
		
        $location= $this->createElement('text', 'location');
		$location->setLabel('Location');
		$location->setAttrib('size', 30);
		$location->setAttrib('class', 'form-control');
		$this->addElement($location);
		
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
    }


}

