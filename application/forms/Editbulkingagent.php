<?php

class Form_Editbulkingagent extends Zend_Form
{

    public function init()
    {
       //Account No
        $names= $this->createElement('text', 'fname');
		$names->setLabel('First Name');
		$names->setAttrib('size', 30);
		$names->setAttrib('class', 'form-control');
		$this->addElement($names);
		
		//Account No
        $mname= $this->createElement('text', 'mname');
		$mname->setLabel('Middle Name');
		$mname->setAttrib('size', 30);
		$mname->setAttrib('class', 'form-control');
		$this->addElement($mname);
		
		//Account No
        $lnames= $this->createElement('text', 'lname');
		$lnames->setLabel('Last Name');
		$lnames->setAttrib('size', 30);
		$lnames->setAttrib('class', 'form-control');
		$this->addElement($lnames);
		
		
		//counties
        $group= $this->createElement('select', 'parentagentid');
		$group->setLabel('Production Group');
		$group->setAttrib('class', 'form-control');
		
		$group->addMultiOption('', '(Choose one)');
		
		$modelAgents = new Model_Agents();
		$agents = $modelAgents->fetchData();
		$array = $agents ->toArray();
		if(count($array)>0){
			foreach ($agents as $agent) {
				$group->addMultiOption($agent->id, $agent->name);
			}
		}else{
			$group->addMultiOption(' ', 'No Groups available');
		}
		$this->addElement($group);
		
		 //Quantity
        $email= $this->createElement('text', 'email');
		$email->setLabel('Email (optional)');
		$email->setAttrib('size', 30);
		$email->setAttrib('class', 'form-control');
		$this->addElement($email);
		
		//Quantity
        $phoneno= $this->createElement('text', 'phoneno');
		$phoneno->setLabel('Phone No.');
		$phoneno->setAttrib('size', 30);
		$phoneno->setAttrib('class', 'form-control');
		$this->addElement($phoneno);
		
		
		//Quantity
        $idno= $this->createElement('text', 'idno');
		$idno->setLabel('National ID No.');
		$idno->setRequired(TRUE);
		$idno->setAttrib('size', 30);
		$idno->setAttrib('class', 'form-control');
		$this->addElement($idno);
		
		
		//submit
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
    }


}

