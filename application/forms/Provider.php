<?php

class Form_Provider extends Zend_Form
{

    public function init()
    {
         //Account No
        $names= $this->createElement('text', 'names');
		$names->setLabel('Name');
		$names->setRequired(TRUE);
		$names->setAttrib('size', 30);
		$names->setAttrib('class', 'form-control');
		$this->addElement($names);
		
		//counties
        $group= $this->createElement('select', 'inputshopid');
		$group->setLabel('Shop');
		$group->setAttrib('class', 'form-control');
		
		$group->addMultiOption('', '(Choose one)');
		
		$modelInpputshops = new Model_Inputshops();
		$inputshops = $modelInpputshops->fetchData();
		$array = $inputshops ->toArray();
		if(count($array)>0){
			foreach ($inputshops as $inputshop) {
				$group->addMultiOption($inputshop->id, $inputshop->name);
			}
		}else{
			$group->addMultiOption(' ', 'No Groups available');
		}
		$this->addElement($group);
		
		
		
		
		 //Quantity
        $email= $this->createElement('text', 'email');
		$email->setLabel('Email (optional)');
		$email->setAttrib('size', 30);
		$email->setAttrib('class', 'form-control');
		$this->addElement($email);
		
		//Quantity
        $phoneno= $this->createElement('text', 'phoneno');
		$phoneno->setLabel('Phone No.');
		$phoneno->setAttrib('size', 30);
		$phoneno->setAttrib('class', 'form-control');
		$this->addElement($phoneno);
		
		
		//Quantity
        $idno= $this->createElement('text', 'idno');
		$idno->setLabel('National ID No.');
		$idno->setRequired(TRUE);
		$idno->setAttrib('size', 30);
		$idno->setAttrib('class', 'form-control');
		$this->addElement($idno);
		
		//submit
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
    }


}

