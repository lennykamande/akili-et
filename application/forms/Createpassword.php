<?php

class Form_Createpassword extends Zend_Form
{

    public function init()
    {
         //Account No
        $pass= $this->createElement('password', 'password');
		$pass->setLabel('New Password');
		$pass->setRequired(TRUE);
		$pass->setAttrib('size', 30);
		$pass->setAttrib('class', 'form-control');
		$this->addElement($pass);
		
		 //Account No
        $cpass= $this->createElement('password', 'cpassword');
		$cpass->setLabel('Confirm new Password');
		$cpass->setRequired(TRUE);
		$cpass->setAttrib('size', 30);
		$cpass->setAttrib('class', 'form-control');
		$this->addElement($cpass);
		
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-primary'));
    }


}

