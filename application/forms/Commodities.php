<?php

class Form_Provider extends Zend_Form
{

    public function init()
    {
        $names= $this->createElement('text', 'productname');
		$names->setLabel('Product Name');
		$names->setRequired(TRUE);
		$names->setAttrib('size', 30);
		$names->setAttrib('class', 'form-control');
		$this->addElement($names);
    
        $quantity= $this->createElement('text', 'Productquantity');
		$quantity->setLabel('Quantity');
		$quantity->setRequired(TRUE);
		$quantity->setAttrib('size', 30);
		$quantity->setAttrib('class', 'form-control');
		$this->addElement($quantity);
    
    
      $price= $this->createElement('text', 'price');
		$price->setLabel('Price');
		$price->setRequired(TRUE);
		$price->setAttrib('size', 30);
		$price->setAttrib('class', 'form-control');
		$price->addElement($price);
       
 ?>