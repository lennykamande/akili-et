<?php

class Form_Exchange extends Zend_Form
{

    public function init()
    {
         //Account No
        $names= $this->createElement('text', 'payee');
		$names->setLabel('Farmer Account');
		$names->setRequired(TRUE);
		$names->setAttrib('size', 30);
		$names->setAttrib('class', 'form-control');
		$names->setAttrib('id', 'accounts');
		$this->addElement($names);
		
		
		 //Quantity
        $email= $this->createElement('text', 'value');
		$email->setLabel('Value');
		$email->setRequired(TRUE);
		$email->setAttrib('size', 30);
		$email->setAttrib('class', 'form-control');
		$this->addElement($email);
		
		
		
		//Quantity
        $idno= $this->createElement('text', 'description');
		$idno->setLabel('Describe service or product issued');
		$idno->setRequired(TRUE);
		$idno->setAttrib('size', 30);
		$idno->setAttrib('class', 'form-control');
		$this->addElement($idno);
		
		//submit
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
    }


}

