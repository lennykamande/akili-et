<?php

class Form_Asset extends Zend_Form
{

    public function init()
    {
        //Account No
        $names= $this->createElement('text', 'name');
		$names->setLabel('Name');
		$names->setRequired(TRUE);
		$names->setAttrib('size', 30);
		$names->setAttrib('class', 'form-control');
		$names->setAttrib('placeholder', 'e.g Milk Can');
		$this->addElement($names);
		
		
		
		// //Quantity
        // $description= $this->createElement('hidden', 'description');
		// $description->setLabel('Description');
		// $description->setAttrib('size', 30);
		// $description->setAttrib('class', 'form-control');
		// $this->addElement($description);
		
		
		//counties
        $group= $this->createElement('select', 'state');
		$group->setLabel('State of the asset');
		$group->setAttrib('class', 'form-control');
		
		$group->addMultiOption('', '(Choose one)');
		$group->addMultiOption('New', 'New');
		$group->setRequired(TRUE);
		$group->addMultiOption('Used before', 'Used before');
		$group->addMultiOption('Broken', 'Broken');
		$group->addMultiOption('Stolen', 'Stolen');
		
		$this->addElement($group);
		
		
		
		
		 //Quantity
        $email= $this->createElement('text', 'value');
		$email->setLabel('Cost (Optional)');
		$email->setAttrib('size', 30);
		$email->setAttrib('class', 'form-control');
		$email->setAttrib('placeholder', 'Ksh');
		$this->addElement($email);
		
		//Quantity
        $phoneno= $this->createElement('text', 'datepurchased');
		$phoneno->setLabel('Date Purchased (optional)');
		$phoneno->setAttrib('size', 30);
		$phoneno->setAttrib('class', 'form-control');
		$phoneno->setAttrib('id', 'datepicker2');
		$phoneno->setAttrib('placeholder', '0000-00-00');
		$this->addElement($phoneno);
		
		
		//submit
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
    }


}

