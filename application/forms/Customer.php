<?php

class Form_Customer extends Zend_Form
{

    public function init()
    {
            //Account No
        $names= $this->createElement('text', 'names');
		$names->setLabel('Full Names');
		$names->setRequired(TRUE);
		$names->setAttrib('size', 30);
		$names->setAttrib('class', 'form-control');
		$this->addElement($names);
		
		 //Quantity
        $email= $this->createElement('text', 'email');
		$email->setLabel('Email (optional)');
		$email->addFilters(array('StringTrim', 'StripTags'));
    	$email->addValidator('EmailAddress',  TRUE  );
		$email->setAttrib('size', 30);
		$email->setAttrib('class', 'form-control');
		$this->addElement($email);
		
		//Quantity
        $phoneno= $this->createElement('text', 'phoneno');
		$phoneno->setLabel('Phone No.');
		$phoneno->addFilters(array('StringTrim', 'StripTags'));
		$phoneno->addFilter(new Zend_Filter_Int());
		$phoneno->setAttrib('size', 30);
		$phoneno->setAttrib('class', 'form-control');
		$this->addElement($phoneno);
		
		
		//Quantity
        $idno= $this->createElement('text', 'idno');
		$idno->setLabel('National ID No.');
		$idno->addFilter(new Zend_Filter_Int());
		$idno->setAttrib('size', 30);
		$idno->setAttrib('class', 'form-control');
		$this->addElement($idno);
		
		
		//submit
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
    }


}

