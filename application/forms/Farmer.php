<?php

class Form_Farmer extends Zend_Form
{

    public function init()
    {
        //Account No
        $names= $this->createElement('text', 'names');
		$names->setLabel('Full Names');
		$names->setRequired(TRUE);
		$names->setAttrib('size', 30);
		$names->setAttrib('class', 'form-control');
		$this->addElement($names);
		
		//counties
        $group= $this->createElement('select', 'parentagentid');
		$group->setLabel('Production Group');
		$group->setAttrib('class', 'form-control');
		
		$group->addMultiOption('', '(Create or Select)');
		
		$modelAgents = new Model_Agents();
		$agents = $modelAgents->fetchData();
		
		$array = $agents ->toArray();
		if(count($array)>0){
			foreach ($agents as $agent) {
				$group->addMultiOption($agent->id, $agent->name);
			}
		}else{
			$group->addMultiOption(' ', 'No Groups available');
		}
		$group->setRequired(TRUE);
		$this->addElement($group);
		
		 //Quantity
        $email= $this->createElement('text', 'email');
		$email->setLabel('Email (optional)');
		$email->setRequired(TRUE);
		$email->setAttrib('size', 30);
		$email->setAttrib('class', 'form-control');
		$this->addElement($email);
		
		//Quantity
        $phoneno= $this->createElement('text', 'phoneno');
		$phoneno->setLabel('Phone No.');
		$phoneno->setAttrib('size', 30);
		$phoneno->setAttrib('class', 'form-control');
		$this->addElement($phoneno);
		
		//Quantity
        $idno= $this->createElement('text', 'idno');
		$idno->setLabel('National ID No.');
		$idno->setRequired(TRUE);
		$idno->setAttrib('size', 30);
		$idno->setAttrib('class', 'form-control');
		$this->addElement($idno);
		
		//counties
        $nyumbakumi= $this->createElement('select', 'nyumbakumi');
		$nyumbakumi->setLabel('Nyumba Kumi');
		$nyumbakumi->addMultiOption('', '(Create or Select)');
		$nyumbakumi->setAttrib('class', 'form-control');
		
		$modelNK= new Model_Nyumbakumi();
		$nyumbakumis = $modelNK->fetchData();
		if($nyumbakumis){
			foreach ($nyumbakumis as $ny) {
				$nyumbakumi->addMultiOption($ny->id, $ny->name);
			}
		}
		$nyumbakumi->setRequired(TRUE);
		$this->addElement($nyumbakumi);
		
		
		
		
		//submit
		$this->addElement('submit', 'submit', array('label' => 'Submit', 'class'=>'btn btn-success'));
    }


}

