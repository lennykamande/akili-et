<?php

class Model_Purchase extends Zend_Db_Table_Abstract
{
	protected $_name='purchases';
 
 	public function addData($data){
 		
		$row = $this->createRow();
		$row->setFromArray($data);
		if(empty($data['datepurchased'])){
			$date = date('Y-m-d H:i:s');
			$row->datepurchased = $date;
		}
		//save the new row
		return $row->save();
		//now fetch the id of the row just created and return it
		$id=$this->_db->lastInsertId();
		return $id;
		}
	public function fetchData(){
			 	$select = $this->select()
								->where('deleted=?',0)
								->order('datepurchased DESC');
				return $this->fetchAll($select);
	}
	public function fetchPurchasesByAccountno($accountno){
			 	$select = $this->select()
								->where('buyerAccount=?',$accountno)
								->order('datepurchased DESC');
				return $this->fetchAll($select);
	}
	
	public function fetchPurchasesBySellerAccount($accountno){
			 	$select = $this->select()
								->where('sellerAccount=?',$accountno)
								->order('datepurchased DESC');
				return $this->fetchAll($select);
	}
	
	
	function updateData($id, $data)
	 {
		
		$select = $this->select()
					->where('id=?',$id);
		$rows= $this->fetchAll($select);
		if(!empty($rows)){
			foreach ($rows as $row) {
			$row->setFromArray($data);
			//save the new row
			return $row->save();
			}
			return TRUE;
		}else{
			return FALSE;
		}
		
       
	 }
	 public function calculateValue($rate, $quantity){
	 	return ($rate * $quantity);
	 }
	 public function fetchPurchasesPerAccount(){
	 	$select = $this->select()
						->setIntegrityCheck(FALSE)
						->from('purchases', array('id', 'sellerAccount','sum(quantity) as quantity','sum(value) as value'))
						->group('sellerAccount')
						->order('datepurchased DESC');
			return $this->fetchAll($select);
	 }

	public function fetchAllSumPurchaseWorth(){
	 	$select = $this->select()
						->setIntegrityCheck(FALSE)
						->from('purchases', array('id', 'sellerAccount','sum(quantity) as quantity','sum(value) as value'))
						->order('datepurchased DESC');
			return $this->fetchRow($select);
	 }

	public function fetchSumPurchasesPerAccount($accountno){
	 	$select = $this->select()
						->where('sellerAccount=?', $accountno)
						->from('purchases', array('id', 'sellerAccount','sum(quantity) as quantity','sum(value) as value'))
						->group('sellerAccount')
						->order('datepurchased DESC');
			return $this->fetchRow($select);
	 }


	public function fetchPurchasesByAgentsPerBA($agentcatid=null, $agentid){
			if($agentcatid==''){
				$agentcatid=102;
			}	
			
			$select = $this->select()
					->from('purchases', array('id','sellerAccount', 'buyerAccount','datepurchased','quantity','rate', 'value'))
							->joinLeft('users',' users.accountno=purchases.buyerAccount',array())
							->where('users.agentcatid=?',$agentcatid)
							->where('users.agentid=?',$agentid)
							->order('datepurchased DESC');
			return $this->fetchAll($select);
	}
	
	public function fetchSumPurchasesByAgentsPerBA($agentcatid=null, $agentid){
			if($agentcatid==''){
				$agentcatid=102;
			}	
			
			$select = $this->select()
					->from('purchases', array('id','sellerAccount', 'buyerAccount','datepurchased','quantity','rate', 'sum(value) as value'))
							->joinLeft('users',' users.accountno=purchases.buyerAccount',array())
							->where('users.agentcatid=?',$agentcatid)
							->group('sellerAccount')
							->where('users.agentid=?',$agentid)
							->order('datepurchased DESC');
			return $this->fetchAll($select);
	}
	
	
	public function fetchOneSumPurchasesByAgentsPerBA($agentcatid=null, $agentid){
			if($agentcatid==''){
				$agentcatid=102;
			}	
			
			$select = $this->select()
					->from('purchases', array('id','sellerAccount', 'buyerAccount','datepurchased','quantity','rate', 'sum(value) as value'))
							->joinLeft('users',' users.accountno=purchases.buyerAccount',array())
							->where('users.agentcatid=?',$agentcatid)
							->group('sellerAccount')
							->where('users.agentid=?',$agentid)
							->order('datepurchased DESC');
			return $this->fetchRow($select);
	}
	
	public function fetchOneSumPurchasesPerShop($agentcatid=null, $shopid){
			if($agentcatid==''){
				$agentcatid=103;
			}	
			$select = $this->select()
					->from('purchases', array('id','sellerAccount', 'buyerAccount','datepurchased','quantity','rate', 'sum(value) as value'))
							->joinLeft('users',' users.accountno=purchases.buyerAccount',array())
							->where('users.agentcatid=?',$agentcatid)
							->group('users.shopid')
							->where('users.shopid=?',$shopid)
							->order('datepurchased DESC');
			return $this->fetchRow($select);
	}
	
	
	
	
	
	
	
	
	public function fetchAgents($agentcatid){
			$select = $this->select()
			 		->setIntegrityCheck(FALSE)
					->from('purchases', array('id', 'buyerAccount'))
							->joinLeft('users',' users.accountno=purchases.buyerAccount',array())
							->where('users.agentcatid=?',$agentcatid)
							->order('datepurchased DESC');
			return $this->fetchAll($select);
	}
	
	public function fetchPurchasesByClerksPerShop($shopid){
			$select = $this->select()
			 		->setIntegrityCheck(FALSE)
					->from('purchases', array('id','sellerAccount','buyerAccount','datepurchased','quantity','rate', 'value'))
							->joinLeft('users',' users.accountno=purchases.buyerAccount',array())
							->where('users.agentcatid=?',103)
							->where('users.shopid=?',$shopid)
							->order('datepurchased DESC');
			return $this->fetchAll($select);
	}
	
	public function fetchSumPurchasesByClerksPerShop($shopid){
			$select = $this->select()
			 		->setIntegrityCheck(FALSE)
					->from('purchases', array('id','sellerAccount','buyerAccount','datepurchased','sum(quantity) as quantity','rate', 'sum(value) as value'))
							->joinLeft('users',' users.accountno=purchases.buyerAccount',array())
							->where('users.agentcatid=?',103)
							->group('sellerAccount')
							->where('users.shopid=?',$shopid)
							->order('datepurchased DESC');
			return $this->fetchAll($select);
	}
	
	
	
	
		 

}

