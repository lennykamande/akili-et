<?php

class Model_Assets extends Zend_Db_Table_Abstract
{
	protected $_name='assets';
 
 	public function addData($data){
		$row = $this->createRow();
		$row->setFromArray($data);
		//save the new row
		return $row->save();
		//now fetch the id of the row just created and return it
		$id=$this->_db->lastInsertId();
		return $id;
		}
	public function fetchData(){
			 	$select = $this->select()
								->where('deleted=?',0)
								->order('datecreated DESC');
				return $this->fetchAll($select);
	}
	
	public function fetchDataByAgentid($agentid){
			 	$select = $this->select()
								->where('agentid=?',$agentid)
								->where('deleted=?',0)
								->order('datecreated DESC');
				return $this->fetchAll($select);
	}
	
	public function fetchDataByShopid($shopid){
		 	$select = $this->select()
							->where('shopid=?',$shopid)
							->where('deleted=?',0)
							->order('datecreated DESC');
			return $this->fetchAll($select);
	}
	
	
	
	

}
