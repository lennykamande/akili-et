<?php

class Model_Exchange extends Zend_Db_Table_Abstract
{
	protected $_name='valueexchange';
 
 	public function addData($data){
		$row = $this->createRow();
		$row->setFromArray($data);
		//save the new row
		return $row->save();
		//now fetch the id of the row just created and return it
		$id=$this->_db->lastInsertId();
		return $id;
		}
	public function fetchData(){
			 	$select = $this->select()
								->where('deleted=?',0)
								->order('datecreated DESC');
				return $this->fetchAll($select);
	}

}

