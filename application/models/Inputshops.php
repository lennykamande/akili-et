<?php

class Model_Inputshops extends Zend_Db_Table_Abstract
{
	protected $_name='inputshops';
 
 public function addData($data){
		$row = $this->createRow();
		$row->setFromArray($data);
		//save the new row
		return $row->save();
		//now fetch the id of the row just created and return it
		$id=$this->_db->lastInsertId();
		return $id;
	}
 function updateGuid($id)
		 {
			
			$select = $this->select()
						->where('id=?',$id);
			$rows= $this->fetchAll($select);
			if(!empty($rows)){
				foreach ($rows as $row) {
				$row->guid = uniqid('et');
				//save the new row
				return $row->save();
				}
				return TRUE;
			}else{
				return FALSE;
			}
	       
		 }
		 
	 function updateData($id, $data)
		 {
			
			$select = $this->select()
						->where('id=?',$id);
			$rows= $this->fetchAll($select);
			if(!empty($rows)){
				foreach ($rows as $row) {
				$row->setFromArray($data);
				//save the new row
				return $row->save();
				}
				return TRUE;
			}else{
				return FALSE;
			}
			
	       
		 }
		 public function fetchById($id){
		 	$select = $this->select()
						->where('deleted=?',0);
			return $this->fetchRow($select);
		 }
		  public function fetchData(){
		 	$select = $this->select()
						->where('deleted=?',0)
						->order('datecreated DESC');
			return $this->fetchAll($select);
		 }


}

