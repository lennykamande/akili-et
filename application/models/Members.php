<?php

class Model_Members extends Zend_Db_Table_Abstract
{
	protected $_name='users';
 
 public function addData($data){
		$row = $this->createRow();
		$row->setFromArray($data);
		$row->guid = uniqid('et');
		$row->accountno = $this->generateAndValidateAccountNo();
		//save the new row
		return $row->save();
		//now fetch the id of the row just created and return it
		$id=$this->_db->lastInsertId();
		return $id;
	}
	 function updateGuid($id)
			 {
				
				$select = $this->select()
							->where('id=?',$id);
				$rows= $this->fetchAll($select);
				if(!empty($rows)){
					foreach ($rows as $row) {
					$row->guid = uniqid('et');
					//save the new row
					return $row->save();
					}
					return TRUE;
				}else{
					return FALSE;
				}
		       
			 }
			 
	public function fetchAgentsPerBulkingAgent($agentid){
		 	$select = $this->select()
						->where('agentcatid=?',102)
						->where('agentid=?',$agentid);
			return $this->fetchAll($select);
		 }
	
	 function updateData($accountno, $data)
		 {
			
			$select = $this->select()
						->where('accountno=?',$accountno);
			$rows= $this->fetchAll($select);
			if(!empty($rows)){
				foreach ($rows as $row) {
				$row->setFromArray($data);
				//save the new row
				return $row->save();
				}
				return TRUE;
			}else{
				return FALSE;
			}
			
	       
		 }
		 public function calculateValue($rate, $quantity){
		 	return ($rate * $quantity);
		 }
		 public function fetchByAccountNo($accountid){
		 	$select = $this->select()
						->where('accountno=?',$accountid);
			return $this->fetchRow($select);
		 }
		 
		 
		 public function fetchByPhoneNo($phoneno){
		 	$select = $this->select()
						->where('phoneno=?',$phoneno);
			return $this->fetchRow($select);
		 }
		 
		 
		 public function fetchByGuid($guid){
		 	$select = $this->select()
						->where('guid=?',$guid);
			return $this->fetchRow($select);
		 }
		 public function fetchByActivationkey($guid){
		 	$select = $this->select()
						->where('activate=?',$guid);
			return $this->fetchRow($select);
		 }
		  public function fetchById($id){
		 	$select = $this->select()
						->where('id=?',$id);
			return $this->fetchRow($select);
		 }
		 public function fetchData(){
		 	$select = $this->select()
							->where('active=?',1)
							->order('datecreated DESC');
			return $this->fetchAll($select);
		 }
		  public function fetchAllData(){
		 	$select = $this->select();
			return $this->fetchAll($select);
		 }
		 public function fetchFarmers(){
		 	$select = $this->select()
							->where('agentcatid=?',101)
							->order('datecreated DESC');
			return $this->fetchAll($select);
		 }
		  public function fetchAdministrators(){
		 	$select = $this->select()
							->where('agentcatid=?',106)
							->order('datecreated DESC');
			return $this->fetchAll($select);
		 }
		 public function fetchBulkingAgents(){
		 	$select = $this->select()
							->where('agentcatid=?',102)
							->where('active=?',1)
							->order('datecreated DESC');
			return $this->fetchAll($select);
		 }
		 
		 public function fetchAgentsByBulkingAgent($agentid){
		 	$select = $this->select()
							->where('agentcatid=?',102)
							->where('agentid=?',$agentid)
							->where('active=?',1)
							->order('datecreated DESC');
			return $this->fetchAll($select);
		 }
		 
		 public function fetchFarmersByAgent($agentid){
		 	$select = $this->select()
							->where('agentcatid=?',101)
							->where('agentid=?',$agentid)
							->order('datecreated DESC');
			return $this->fetchAll($select);
		 }
		 
		  public function fetchCustomersByBulkingAgent($agentid){
		 	$select = $this->select()
							->where('agentcatid=?',105)
							->where('agentid=?',$agentid)
							->order('datecreated DESC');
			return $this->fetchAll($select);
		 }
		 
		 
		  public function fetchClerksByShop($shopid){
		 	$select = $this->select()
							->where('agentcatid=?',103)
							->where('shopid=?',$shopid)
							->where('active=?',1)
							->order('datecreated DESC');
			return $this->fetchAll($select);
		 }
		  
		  public function fetchCustomersByShop($shopid){
		 	$select = $this->select()
							->where('agentcatid=?',105)
							->where('shopid=?',$shopid)
							->order('datecreated DESC');
			return $this->fetchAll($select);
		 }
		 
		 
		 public function fetchShopAttendants(){
		 	$select = $this->select()
							->where('agentcatid=?',103)
							->where('active=?',1)
							->order('datecreated DESC');
			return $this->fetchAll($select);
		 }
		 public function fetchInputShopMembers(){
		 	$select = $this->select()
							->where('agentcatid=?',104)
							->where('active=?',1)
							->order('datecreated DESC');
			return $this->fetchAll($select);
		 }
		  public function fetchCustomers(){
		 	$select = $this->select()
							->where('agentcatid=?',105)
							->where('deleted=?',0)
							->order('datecreated DESC');
			return $this->fetchAll($select);
		 }
		  
		  public function fetchLoginStatus($email){
				$select = $this->select()
			 		->setIntegrityCheck(FALSE)
					->from('users', array('loginstatus'))
							->where('loginstatus=?',1)
							->where('email=?',$email);
				return $this->fetchRow($select);
			}
		  
		  public function checkCanLogin($email){
		  	$member = $this->fetchLoginStatus($email);
			if($member){
				if($member->loginstatus==1){
					return 1;
					
				}else{
					return 0;
				}
			}else{
				return 0;
			}
		  }
		  
		  
		 public function checkIfExists($accountid){
		 	$select = $this->select()
							->where('accountno=?',$accountid);
			$result=$this->fetchRow($select);
			if($result){
				return 1;
			}else{
				return 0;
			}
		 }
		 
		 
		 public function checkIfNameExists($fname, $mname=null, $lname=null){
		 	if(isset($mname) && isset($lname)){
		 		$select = $this->select()
							->where('fname=?',$fname)
							->where('mname=?',$mname)
							->where('lname=?',$lname);
		 	}elseif(isset($mname) && isset($fname)){
		 		$select = $this->select()
							->where('fname=?',$fname)
							->where('mname=?',$mname);
		 	}else{
		 		$select = $this->select()
							->where('fname=?',$fname);
		 	}
			
			$member=$this->fetchRow($select);
			
			if($member){
				return $member;
			}else{
				return null;
			}
		 }


		 
		 public function checkifaccountexists($email=null, $phone=null){
		 	if(isset($email)){
		 		$select = $this->select()
							->where('email=?',$email);
				$result=$this->fetchRow($select);
				
				if($result){
				return 1;
			}else{
				//check of phone no is already registered
				if(isset($phone)){
					$select = $this->select()
								->where('phoneno=?',$phone);
					$re=$this->fetchRow($select);
					if($re){
						return 1;
					}else{
						return 0;
					}
				}
			}
			
		 	}else{
		 		if(isset($phone)){
					$select = $this->select()
								->where('phoneno=?',$phone);
					$re=$this->fetchRow($select);
					if($re){
						return 1;
					}else{
						return 0;
					}
				}else{
					return 0;
				}
		 	}
			
			
		 }
		 public function generateAndValidateAccountNo(){
		 	$min = 100000;
			$max=999999;	
			//check of taken
			//chose for loop to prevent it from looping forever
			for ($i=100000; $i < 999999 ; $i++) {
					 
				$accountNo = rand($min, $max);
				
				$result = $this->checkIfExists($accountNo);
				
				if(!$result){
					return $accountNo;
					exit;
				}
			}
			
			
		 }
		 public function fragmentNames($account){
		 	//if only name is provided
					//break the spaced name
					$accountArray = explode(' ', $account);
					
					if(isset($accountArray['0'])){
						$data['fname'] =  $accountArray['0'];
						array_push($data,$data['fname']);
					}
					if(isset($accountArray['1'])){
						$data['mname'] =  $accountArray['1'];
						array_push($data,$data['mname']);
					}
					if(isset($accountArray['2'])){
						$data['lname'] =  $accountArray['2'];
						array_push($data,$data['lname']);
					}
					
					return $data;
			
		 }
		 
		  public function fetchShopAttendantsByShop($shopid){
		 	$select = $this->select()
							->where('agentcatid=?',103)
							->where('shopid=?',$shopid)
							->order('datecreated DESC');
			return $this->fetchAll($select);
		 }
		  
		 

}
