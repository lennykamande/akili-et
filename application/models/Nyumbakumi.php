<?php

class Model_Nyumbakumi extends Zend_Db_Table_Abstract
{
protected $_name='nyumbakumi';
 
 public function addData($data){
		$row = $this->createRow();
		$row->setFromArray($data);
		//save the new row
		return $row->save();
		//now fetch the id of the row just created and return it
		$id=$this->_db->lastInsertId();
		return $id;
	}
	 function updateData($id, $data)
		 {
			
			$select = $this->select()
						->where('id=?',$id);
			$rows= $this->fetchAll($select);
			if(!empty($rows)){
				foreach ($rows as $row) {
				$row->setFromArray($data);
				//save the new row
				return $row->save();
				}
				return TRUE;
			}else{
				return FALSE;
			}
			
	       
		 }
		 public function fetchPaymentsByPayee($accountid){
		 	$select = $this->select()
							->where('payee=?',$accountid)
							->where('deleted=?',0)
							->setIntegrityCheck(FALSE)
							->from('payments', array('id', 'payee','sum(value) as value'))
							->group('payee');
							
				return $this->fetchRow($select);
		 }
		  public function fetchData(){
		 	$select = $this->select()
							->where('active=?',1);
				return $this->fetchAll($select);
		 }
}

