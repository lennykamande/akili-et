<?php

class Model_Sales extends Zend_Db_Table_Abstract
{
	protected $_name='sales';
 
 	public function addData($data){
		$row = $this->createRow();
		$row->setFromArray($data);
		if(empty($data['datesold'])){
			$date = date('Y-m-d H:i:s');
			$row->datesold = $date;
		}
		
		//save the new row
		return $row->save();
		}
	public function fetchData(){
			 	$select = $this->select()
								->where('deleted=?',0)
								->order('datesold DESC');
				return $this->fetchAll($select);
	}
	function updateData($id, $data)
	 {
		
		$select = $this->select()
					->where('id=?',$id);
		$rows= $this->fetchAll($select);
		if(!empty($rows)){
			foreach ($rows as $row) {
			$row->setFromArray($data);
			//save the new row
			return $row->save();
			}
			return TRUE;
		}else{
			return FALSE;
		}
		
       
	 }
	 
	 public function calculateValue($rate, $quantity){
	 	return ($rate * $quantity);
	 }
	 
	 public function fetchSalesByAccountno($accountno){
			 	$select = $this->select()
								->where('sellerAccount=?',$accountno)
								->order('datesold DESC');
				return $this->fetchAll($select);
	}
	 public function fetchSalesToAccountno($accountno){
			 	$select = $this->select()
								->where('customerAccount=?',$accountno)
								->order('datesold DESC');
				return $this->fetchAll($select);
	}
	  public function fetchSalesByCustomerAccountno($accountno){
			 	$select = $this->select()
								->where('customerAccount=?',$accountno)
								->order('datesold DESC');
				return $this->fetchAll($select);
	}
	 
	 public function fetchSalesByAgentsPerBA($agentid){
			$select = $this->select()
					->from('sales', array('id', 'sellerAccount','datesold','quantity','rate', 'value','customerAccount'))
							->joinLeft('users',' users.accountno=sales.sellerAccount',array())
							->where('users.agentcatid=?',102)
							->where('users.agentid=?',$agentid)
							->order('datesold DESC');
			return $this->fetchAll($select);
	}
	 public function fetchAllSumSalesWorth(){
			$select = $this->select()
					->from('sales', array('id', 'sellerAccount','datesold','quantity','rate', 'sum(value) as value','customerAccount'));
			return $this->fetchRow($select);
	}
	 
	 
	 
	 public function fetchOneSumPurchasesByAgentsPerBA($agentcatid=null, $agentid){
			if($agentcatid==''){
				$agentcatid=102;
			}	
			
			$select = $this->select()
					->from('sales', array('id', 'sellerAccount','datesold','quantity','rate', 'sum(value) as value','customerAccount'))
							->joinLeft('users',' users.accountno=sales.sellerAccount',array())
							->where('users.agentcatid=?',$agentcatid)
							->group('sellerAccount')
							->where('users.agentid=?',$agentid)
							->order('datesold DESC');
			return $this->fetchRow($select);
	}
	 
	 
	 public function fetchOneSumPurchasesPerShop($agentcatid=null, $agentid){
			if($agentcatid==''){
				$agentcatid=103;
			}	
			
			$select = $this->select()
					->from('sales', array('id', 'sellerAccount','datesold','quantity','rate', 'sum(value) as value','customerAccount'))
							->joinLeft('users',' users.accountno=sales.sellerAccount',array())
							->where('users.agentcatid=?',$agentcatid)
							->group('users.shopid')
							->where('users.shopid=?',$agentid)
							->order('datesold DESC');
			return $this->fetchRow($select);
	}
	 
	
	 
	 

 public function fetchSalesByClerksPerShop($shopid){
			$select = $this->select()
			 		->setIntegrityCheck(FALSE)
					->from('sales', array('id', 'sellerAccount','datesold','quantity','rate', 'value','customerAccount'))
							->joinLeft('users',' users.accountno=sales.sellerAccount',array())
							->where('users.agentcatid=?',103)
							->where('users.shopid=?',$shopid)
							->order('datesold DESC');
			return $this->fetchAll($select);
	}

}

