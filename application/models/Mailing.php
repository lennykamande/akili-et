<?php

class Model_Mailing extends Zend_Db_Table_Abstract
{
	protected $_name="maillogs";	
	
	public function sendmail($from=NULL,$to,$msg, $subject){
				
				
			$message='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
						<html xmlns="http://www.w3.org/1999/xhtml">
						    <head>
						        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
						        <meta name="viewport" content="width=device-width, initial-scale=1.5"/>
						        <title>SCGIS Email</title>
						
						        
						        <style type="text/css">
									/*////// RESET STYLES //////*/
									body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}
									table{border-collapse:collapse;}
									img, a img{border:0; outline:none; text-decoration:none;}
									h1, h2, h3, h4, h5, h6{margin:0; padding:0;}
									p{margin: 1em 0;}
						
									/*////// CLIENT-SPECIFIC STYLES //////*/
									.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail/Outlook.com to display emails at full width. */
									.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail/Outlook.com to display line heights normally. */
									table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up. */
									#outlook a{padding:0;} /* Force Outlook 2007 and up to provide a "view in browser" message. */
									img{-ms-interpolation-mode: bicubic;} /* Force IE to smoothly render resized images. */
									body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
						
									/*////// GENERAL STYLES //////*/
									body, #bodyTable{background-color:#e7e6e6;}
									#bodyCell{padding-top:20px; padding-bottom:40px;}
									#emailBody{background-color:#FFFFFF; border:1px solid #DDDDDD; border-collapse:separate; border-radius:4px;}
									.flexibleContainerCell{padding-top:20px; padding-right:20px; padding-left:20px;}
									.flexibleImage{height:auto;}
									.bottomShim{padding-bottom:20px;}
						
									h1, h2, h3, h4, h5, h6{color:#202020; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left;}
						             p{font-family:Arial, Helvetica, sans-serif;}
									.textContent, .textContentLast{color:#404040; font-family:Helvetica; font-size:16px; line-height:125%; text-align:Left; padding-bottom:20px;}
									.textContent a, .textContentLast a{color:#2C9AB7; text-decoration:underline;}
						
									.imageContent, .imageContentLast{padding-bottom:20px;}
						
									.nestedContainer{background-color:#E5E5E5; border:1px solid #CCCCCC;}
									.nestedContainerCell{padding-top:20px; padding-Right:20px; padding-Left:20px;}
						
									.emailButton{background-color:#2C9AB7; border-collapse:separate; border-radius:4px;}
									.buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}
									.buttonContent a{color:#FFFFFF; display:block; text-decoration:none; width:100%;}
						
									/*////// MOBILE STYLES //////*/
									@media only screen and (max-width: 480px){			
										/*////// CLIENT-SPECIFIC STYLES //////*/
										body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */
						
										/*////// GENERAL STYLES //////*/
										td[id="bodyCell"]{padding-top:10px !important; padding-Right:10px !important; padding-Left:10px !important;}
										table[id="emailBody"]{width:100% !important;}
						
										table[class="flexibleContainer"]{display:block !important; width:100% !important;}
										img[class="flexibleImage"]{width:100% !important;}
										table[class="emailButton"]{width:100% !important;}
						
										td[class="textContentLast"], td[class="imageContentLast"]{padding-top:20px !important;}
									}
								</style>
						        <!--
						        	Outlook Conditional CSS
						
						            These two style blocks target Outlook 2007 & 2010 specifically, forcing
						            columns into a single vertical stack as on mobile clients. This is
						            primarily done to avoid the \'page break bug\' and is optional.
						
						            More information here:
									http://templates.mailchimp.com/development/css/outlook-conditional-css
						        -->
						        <!--[if mso 12]>
						            <style type="text/css">
						            	.flexibleContainer{display:block !important; width:100% !important;}
						            </style>
						        <![endif]-->
						        <!--[if mso 14]>
						            <style type="text/css">
						            	.flexibleContainer{display:block !important; width:100% !important;}
						            </style>
						        <![endif]-->
						    </head>
						    <body>
						    	
						    	<center>
						        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
						        	    <tr>
						        	       <td align="center" valign="top" style="background-color:#4CAF3B; padding-top:10px; padding-bottom:10px;">
						        	            <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
						        	                <tr>
						        	                    <td>
						        	                         <a style="text-decoration:none; font-size:20px; font-weight:normal; color:#ffffff; padding-bottom=10px; font-family:Arial, Helvetica, sans-serif;" href="http://www.akilidev.org">  Akili eT</a>
						        	                    </td>
						        	                    <td align="right" style="padding-right:5px;">
						                                     <a style="text-decoration:none; font-size:14px; font-weight:normal; color:#ffffff; padding-bottom=10px; font-family:Arial, Helvetica, sans-serif;" href="http://www.akiliet.com">www.akiliet.com</a>
						                                </td>
						        	                </tr>
						        	            </table>
						        	            
						        	           
						        	        </td>
						        	    </tr>
						            	<tr>
						                	<td align="center" valign="top" id="bodyCell">
						                    	<!-- EMAIL CONTAINER // -->
						                    	<table border="0" cellpadding="0" cellspacing="0" width="600" id="emailBody">
						
						
													<!-- MODULE ROW // -->
						                            <!--
						                            	To move or duplicate any of the design patterns
						                                in this email, simply move or copy the entire
						                                MODULE ROW section for each content block.
						                            -->
													<tr>
						                            	<td align="center" valign="top">
						                                	<!-- CENTERING TABLE // -->
						                                	<table border="0" cellpadding="0" cellspacing="0" width="100%">
						                                    	<tr>
						                                        	<td align="center" valign="top">
						                                            	<!-- FLEXIBLE CONTAINER // -->
						                                            	<table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
						                                                	<tr>
						                                                    	<td align="center" valign="top" width="600" class="flexibleContainerCell">
						
						
						                                                            <!-- CONTENT TABLE // -->
						                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
						                                                                <tr>
						                                                                    <td valign="top" class="textContent">
						                                                                    	
						                                                                    	'.$msg.'
						                                                                    	
						                                                                    </td>
						                                                                </tr>
						                                                            </table>
						                                                            <!-- // CONTENT TABLE -->
						
						
						                                                        </td>
						                                                    </tr>
						                                                </table>
						                                                <!-- // FLEXIBLE CONTAINER -->
						                                            </td>
						                                        </tr>
						                                    </table>
						                                    <!-- // CENTERING TABLE -->
						                                </td>
						                            </tr>
						
						
						
						
						
						                        </table>
						                    	<!-- // EMAIL CONTAINER -->
						                    </td>
						                </tr>
						               <!-- // Footer here -->
						                <tr>
						                   <td align="center" valign="top" style="padding-bottom:10px;">
						                        <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
						                            <tr>
						                                <td align="center">
						                                    <!-- CONTENT TABLE // -->
						                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
						                                            <tr>
						                                                <td align="center">
						                                                    <p style="color:#a9a9a9; text-align: center; font-size: 14px;">
						                                                        Akili eT does this and this and this
						                                                       <br/> 
						                                                       C2, Bemuda Plaza, Next to Nairobi Baptist Church, Ngong Road , Nairobi , Kenya
						                                                       <br/> <br/> 
						                                                       © 2014 Akili eT 
						                                                     </p>
						                                                    <br />
						                                                    
						                                                </td>
						                                            </tr>
						                                            
						                                        </table>
						                                        <!-- // CONTENT TABLE -->
						                                </td>
						                               
						                            </tr>
						                        </table>
						                        
						                       
						                    </td>
						                </tr>
						                
						            </table>
						        </center>
						    </body>
						</html>';	
				
			$url = 'http://sendgrid.com/';
				$user = 'nudcon';
				$pass = 'machakos.,';
				
				$cc='emaillogs@taiafricaelectives.com';
				if(!isset($from)){
					$from = INFOEMAIL;
				}
		        //send email to applicant
		        $params = array(
					    'api_user'  => $user,
					    'api_key'   => $pass,
					    'to'        => $to,
					    'cc'        => $cc,
					    'subject'   => $subject,
					    'html'      => $message,
					    'text'      => strip_tags($msg),
					    'from'      => $from,
					    'fromname'  => FROMNAME,
					  );
					
					//include 'sendgrid/SendGrid_loader.php';
					$request =  $url.'api/mail.send.json';
					
					// Generate curl request
					$session = curl_init($request);
					// Tell curl to use HTTP POST
					curl_setopt ($session, CURLOPT_POST, true);
					// Tell curl that this is the body of the POST
					curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
					// Tell curl not to return headers, but do return the response
					curl_setopt($session, CURLOPT_HEADER, false);
					curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
					
					// obtain response
					$response = curl_exec($session);
					curl_close($session);	
					return $response;
					
		}
	 	


}

