<?php

class Model_Rates extends Zend_Db_Table_Abstract
{
	protected $_name='rates';
 
 	public function addData($data){
		$row = $this->createRow();
		$row->setFromArray($data);
		//save the new row
		return $row->save();
		//now fetch the id of the row just created and return it
		}
	public function fetchData(){
			 	$select = $this->select()
								->where('deleted=?',0)
								->order('datecreated DESC');
				return $this->fetchAll($select);
	}
	public function fetchBuyingRatesByOrg($orgid){
			 	$select = $this->select()
								->where('agentid=?',0)
								->order('datecreated DESC');
				return $this->fetchAll($select);
	}
	public function fetchSellingRatesByOrg($orgid){
			 	$select = $this->select()
								->where('agentid=?',$orgid)
								->order('datecreated DESC');
				return $this->fetchAll($select);
	}
	
	public function fetchbuyingrates(){
			 	$select = $this->select()
								->where('type=?','buying')
								->where('deleted=?',0)
								->order('datecreated DESC');
				return $this->fetchAll($select);
	}
	
	public function fetchbuyingratesByAgent($id){
			 	$select = $this->select()
								->where('type=?','buying')
								->where('deleted=?',0)
								->where('agentid=?',$id)
								->order('datecreated DESC');
				return $this->fetchAll($select);
	}
	
	public function fetchSellingratesByAgent($id){
			 	$select = $this->select()
								->where('type=?','selling')
								->where('deleted=?',0)
								->where('agentid=?',$id)
								->order('datecreated DESC');
				return $this->fetchAll($select);
	}



	
	public function fetchSellingratesByShop($id){
			 	$select = $this->select()
								->where('type=?','selling')
								->where('deleted=?',0)
								->where('shopid=?',$id)
								->order('datecreated DESC');
				return $this->fetchAll($select);
	}
	
	public function fetchBuyingratesByShop($id){
			 	$select = $this->select()
								->where('type=?','buying')
								->where('deleted=?',0)
								->where('shopid=?',$id)
								->order('datecreated DESC');
				return $this->fetchAll($select);
	}
	
	
	public function fetchsellingrates(){
			 	$select = $this->select()
								->where('type=?','selling')
								->where('deleted=?',0)
								->order('datecreated DESC');
				return $this->fetchAll($select);
	}

	public function fetchLatestAgentSellingRate($id){
			 	$select = $this->select()
								->where('type=?','selling')
								->where('deleted=?',0)
								->where('agentid=?',$id)
								->order('datecreated DESC')
								->limit(1);
				return $this->fetchRow($select);
	}
	
	public function fetchLatestAgentbuyingRate($id){
			 	$select = $this->select()
								->where('type=?','buying')
								->where('deleted=?',0)
								->where('agentid=?',$id)
								->order('datecreated DESC')
								->limit(1);
				return $this->fetchRow($select);
	}

	
	
	public function fetchLatestShopbuyingRate($id){
			 	$select = $this->select()
								->where('type=?','buying')
								->where('deleted=?',0)
								->where('shopid=?',$id)
								->order('datecreated DESC')
								->limit(1);
				return $this->fetchRow($select);
	}
	
	public function fetchLatestShopSellingRate($id){
			 	$select = $this->select()
								->where('type=?','selling')
								->where('deleted=?',0)
								->where('shopid=?',$id)
								->order('datecreated DESC')
								->limit(1);
				return $this->fetchRow($select);
	}
	
	

}

