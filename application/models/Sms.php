<?php

class Model_Sms extends Zend_Db_Table_Abstract
{
	protected $_name='smslogs';
 
 	public function addData($data){
		$row = $this->createRow();
		$row->setFromArray($data);
		//save the new row
		return $row->save();
		//now fetch the id of the row just created and return it
		$id=$this->_db->lastInsertId();
		return $id;
		}
	public function fetchData(){
			 	$select = $this->select()
								->where('deleted=?',0)
								->order('datesold DESC');
				return $this->fetchAll($select);
	}
	function updateData($id, $data)
	 {
		
		$select = $this->select()
					->where('id=?',$id);
		$rows= $this->fetchAll($select);
		if(!empty($rows)){
			foreach ($rows as $row) {
			$row->setFromArray($data);
			//save the new row
			return $row->save();
			}
			return TRUE;
		}else{
			return FALSE;
		}
		
       
	 }
	 
	 public function fetchAllSMS(){
			 	$select = $this->select();
				return $this->fetchAll($select);
	}
	 public function formatPhoneno($phoneno=null){
 		if($phoneno && is_numeric($phoneno)){
 			$phone = substr($phoneno, -9);
			$toPhone = '0'.$phone;
			return $toPhone;
 		}else{
 			return 0;
 		}
	 		
	 	
	 	
	 }
	 public function sendSMS($to, $message){
	 		
	 	$api_key = '427PTUQEZ9N3NZPZ2AAKD4M6QHFREADG';
	    $project_id = 'PJb1da250d39152eb3';
	    $phone_id = 'PNb56c11b2ad156c38';    
	    $to_number = $to;
	    $content = strip_tags($message);
	    
	    $curl = curl_init();
	    curl_setopt($curl, CURLOPT_URL, 
	        "https://api.telerivet.com/v1/projects/$project_id/messages/outgoing");
	    curl_setopt($curl, CURLOPT_USERPWD, "{$api_key}:");  
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
	        'content' => $content,
	        'phone_id' => $phone_id,
	        'to_number' => $to_number,
	    ), '', '&'));        
	    
	    // if you get SSL errors, download SSL certs from https://telerivet.com/_media/cacert.pem .
	    // curl_setopt($curl, CURLOPT_CAINFO, dirname(__FILE__) . "/cacert.pem");    
	    
	    $json = curl_exec($curl);    
	    $network_error = curl_error($curl);    
	    curl_close($curl);    
	        
	    if ($network_error) { 
	       // echo $network_error; // do something with the error message
	       return 0;
	    } else {
	        $res = json_decode($json, true);
	        
	        if (isset($res['error'])) {
	            // API error
	           // var_dump($res); // do something with the response
	            return 0;
	        } else {            
	            // success! 
	            //log the message 
	            
	            //return true
	            return 1;
	            //var_dump($res); // do something with the response
	        }
	    }
	 }

}

