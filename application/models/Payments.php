<?php

class Model_Payments  extends Zend_Db_Table_Abstract
{
protected $_name='payments';
 
 public function addData($data){
		$row = $this->createRow();
		$row->setFromArray($data);
		//save the new row
		return $row->save();
		//now fetch the id of the row just created and return it
		$id=$this->_db->lastInsertId();
		return $id;
	}
	 function updateData($id, $data)
		 {
			
			$select = $this->select()
						->where('id=?',$id);
			$rows= $this->fetchAll($select);
			if(!empty($rows)){
				foreach ($rows as $row) {
				$row->setFromArray($data);
				//save the new row
				return $row->save();
				}
				return TRUE;
			}else{
				return FALSE;
			}
			
	       
		 }
		 public function fetchPaymentsByPayee($accountid){
		 	$select = $this->select()
							->where('payee=?',$accountid)
							->order('datepaid DESC');
							
				return $this->fetchAll($select);
		 }
		 
		 public function fetchPaymentsByAgentsPerBA($agentcatid=null, $agentid){
			if($agentcatid==''){
				$agentcatid=102;
			}
			$select = $this->select()
			 		->setIntegrityCheck(FALSE)
					->from('payments', array('id','payee', 'payer','value','paymenttype','description', 'datepaid'))
							->joinLeft('users',' users.accountno=payments.payer',array())
							->where('users.agentcatid=?',$agentcatid)
							->where('users.agentid=?',$agentid)
							->order('datepaid DESC');
			return $this->fetchAll($select);
			}
		 
		  public function fetchPaymentsByClerksPerShop($agentcatid=null, $shopid){
			if($agentcatid==''){
				$agentcatid=103;
			}
			$select = $this->select()
			 		->setIntegrityCheck(FALSE)
					->from('payments', array('id','payee', 'payer','value','paymenttype','description', 'datepaid'))
							->joinLeft('users',' users.accountno=payments.payer',array())
							->where('users.agentcatid=?',$agentcatid)
							->where('users.shopid=?',$shopid)
							->order('datepaid DESC');
			return $this->fetchAll($select);
			}
		 
		 
		 
		 public function fetchSumPaymentsByPayee($accountid){
		 	$select = $this->select()
							->where('payee=?',$accountid)
							->where('deleted=?',0)
							->from('payments', array('id', 'payee','sum(value) as value', 'datepaid'))
							->group('payee')
							->order('datepaid DESC');
							
				return $this->fetchRow($select);
		 }
		 
		 public function fetchSumExchangesByPayee(){
		 	$select = $this->select()
							->where('deleted=?',0)
							->where('paymenttype=?','exchange')
							->from('payments', array('id', 'payee','payer','sum(value) as value', 'datepaid','description'))
							->group('payee')
							->order('datepaid DESC');
							
				return $this->fetchAll($select);
		 }
		 public function fetchSumExchangesForpayee($accountno){
		 	$select = $this->select()
							->where('deleted=?',0)
							->where('payee=?',$accountno)
							->where('paymenttype=?','exchange')
							->from('payments', array('id', 'payee','payer','sum(value) as sum', 'datepaid','description'))
							->group('payee')
							->order('datepaid DESC');
							
				return $this->fetchRow($select);
		 }


		public function fetchSumExchangesPerpayer($accountno){
			 	$select = $this->select()
								->where('deleted=?',0)
								->where('payer=?',$accountno)
								->where('paymenttype=?','exchange')
								->from('payments', array('id', 'payee','payer','sum(value) as sum', 'datepaid','description'))
								->group('payee')
								->order('datepaid DESC');
								
					return $this->fetchRow($select);
			 }	
		
		public function fetchExchangesPerpayer($accountno){
			 	$select = $this->select()
								->where('deleted=?',0)
								->where('payer=?',$accountno)
								->where('paymenttype=?','exchange')
								->from('payments', array('id', 'payee','payer','sum(value) as sum', 'datepaid','description'))
								->order('datepaid DESC');
								
					return $this->fetchRow($select);
			 }		 
		 public function fetchSumPaymentsPerPayee($accountno){
		 	$select = $this->select()
							->where('deleted=?',0)
							->where('payee=?',$accountno)
							->from('payments', array('id', 'payee','payer','sum(value) as sum', 'datepaid','description'))
							->group('payee')
							->order('datepaid DESC');
							
				return $this->fetchRow($select);
		 }
		 
		 
		 
		 
		 
		 public function fetchExchangesByPayee($accountno){
		 	$select = $this->select()
							->where('deleted=?',0)
							->where('payee=?',$accountno)
							->where('paymenttype=?','exchange')
							->from('payments', array('id', 'payee','payer','value', 'datepaid','description'))
							->order('datepaid DESC');
				return $this->fetchAll($select);
		 }
		 
		 
		 
		 
		 public function calculateValue($rate, $quantity){
		 	return ($rate * $quantity);
		 }
		 
		 public function fetchByAccountNo($accountid){
		 	$select = $this->select()
						->where('accountno=?',$accountid);
			return $this->fetchRow($select);
		 }
		 public function fetchData(){
		 	$select = $this->select()
							->where('deleted=?',0)
							->order('datepaid DESC');
			return $this->fetchAll($select);
		 }
		 public function checkIfExists($accountid){
		 	$select = $this->select()
							->where('accountno=?',$accountid);
			$result=$this->fetchRow($select);
			if($result){
				return 1;
			}else{
				return 0;
			}
		 }
		 public function generateAndValidateAccountNo(){
		 	$min = 100000;
			$max=999999;	
			
			//check of taken
			//chose for loop to prevent it from looping forever
			for ($i=100000; $i < 999999 ; $i++) { 
				$accountNo = rand($min, $max);
				$result = $this->checkIfExists($accountNo);
				if(!$result){
					return $accountNo;
					exit;
				}
			}
			
			
		 }
		 

}

