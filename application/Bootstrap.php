<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initAutoload()
		{
			// Add autoloader empty namespace
			$autoLoader = Zend_Loader_Autoloader::getInstance();
			$resourceLoader = new Zend_Loader_Autoloader_Resource(array(
			'basePath' => APPLICATION_PATH,
			'namespace' => '',
			'resourceTypes' => array(
			'form' => array(
			'path' => 'forms/',
			'namespace' => 'Form_',
			),
			'model' => array(
			'path' => 'models/',
			'namespace' => 'Model_'
			),
			),
			));
			// Return it so that it can be stored by the bootstrap
			return $autoLoader;
		}
	
	protected function _initViewHelpers() {
    	$view = new Zend_View();
    	$view->headTitle('Akili')->setSeparator(' | ');
	}

}

/** Routing Info for  urls **/
$FrontController = Zend_Controller_Front::getInstance(); 
$Router = $FrontController->getRouter();



 /*
 * 
 * Website routes
 * 
 */

//login
$Router->addRoute("login",
			new Zend_Controller_Router_Route
			(
			"/login",
			array
			("controller" => "account",
			"action" => "login") ));
			
			//register // to be removed
$Router->addRoute("register",
			new Zend_Controller_Router_Route
			(
			"/register",
			array
			("controller" => "account",
			"action" => "register") ));
			
			
			//new member
$Router->addRoute("member/new",
			new Zend_Controller_Router_Route
			(
			"/member/new",
			array
			("controller" => "member",
			"action" => "new") ));
			
			//Purchases
$Router->addRoute("purchases",
			new Zend_Controller_Router_Route
			(
			"/purchases",
			array
			("controller" => "purchase",
			"action" => "index") ));
			
			//sales
$Router->addRoute("sales",
			new Zend_Controller_Router_Route
			(
			"/sales",
			array
			("controller" => "sale",
			"action" => "index") ));
			
			//sales
$Router->addRoute("payments",
			new Zend_Controller_Router_Route
			(
			"/payments",
			array
			("controller" => "payment",
			"action" => "index") ));
			
			//sales
$Router->addRoute("members",
			new Zend_Controller_Router_Route
			(
			"/members",
			array
			("controller" => "member",
			"action" => "index") ));
			
			//sales
$Router->addRoute("farmers",
			new Zend_Controller_Router_Route
			(
			"/members/farmers",
			array
			("controller" => "member",
			"action" => "farmers") ));
			
			//sales
$Router->addRoute("farmerdetails",
			new Zend_Controller_Router_Route
			(
			"/member/farmerdetails/:accountno",
			array
			("controller" => "member",
			"action" => "farmerdetails") ));
			
			//sales
$Router->addRoute("editdetails",
			new Zend_Controller_Router_Route
			(
			"/member/editfarmer/:accountno",
			array
			("controller" => "member",
			"action" => "editfarmer") ));
			
			//sales
$Router->addRoute("deletemember",
			new Zend_Controller_Router_Route
			(
			"/member/delete/:accountno",
			array
			("controller" => "member",
			"action" => "delete") ));
			

			
			//sales
$Router->addRoute("customers",
			new Zend_Controller_Router_Route
			(
			"/members/customers",
			array
			("controller" => "member",
			"action" => "customers") ));
			
			//sales
$Router->addRoute("bulking-agents",
			new Zend_Controller_Router_Route
			(
			"/members/bulking-agents",
			array
			("controller" => "member",
			"action" => "bulking-agents") ));
			
			//sales
$Router->addRoute("bulking-agents-details",
			new Zend_Controller_Router_Route
			(
			"/members/bulking-agent-details/:accountno",
			array
			("controller" => "member",
			"action" => "bulking-agent-details") ));
			
			
			//sales
$Router->addRoute("editagent",
			new Zend_Controller_Router_Route
			(
			"/member/editagent/:accountno",
			array
			("controller" => "member",
			"action" => "editagent") ));
			
			
			
			//sales
$Router->addRoute("farmer-groups",
			new Zend_Controller_Router_Route
			(
			"/members/farmer-groups",
			array
			("controller" => "member",
			"action" => "farmer-groups") ));
			
			//sales
$Router->addRoute("newFarmer",
			new Zend_Controller_Router_Route
			(
			"/member/new",
			array
			("controller" => "member",
			"action" => "new") ));
			
			//sales
$Router->addRoute("providers",
			new Zend_Controller_Router_Route
			(
			"/members/providers",
			array
			("controller" => "member",
			"action" => "providers") ));
			
			
			//sales
$Router->addRoute("shop-attendants",
			new Zend_Controller_Router_Route
			(
			"/members/shop-attendants",
			array
			("controller" => "member",
			"action" => "shop-attendants") ));
			
			
			//sales
$Router->addRoute("agentdetails",
			new Zend_Controller_Router_Route
			(
			"/agents/details/:id",
			array
			("controller" => "agents",
			"action" => "details") ));
			
				//sales
$Router->addRoute("shopdetails",
			new Zend_Controller_Router_Route
			(
			"/shops/details/:guid",
			array
			("controller" => "shops",
			"action" => "details") ));
			
			
			//sales
$Router->addRoute("customerdetails",
			new Zend_Controller_Router_Route
			(
			"/customers/details/:guid",
			array
			("controller" => "customers",
			"action" => "details") ));
			
			//sales
$Router->addRoute("editshopss",
			new Zend_Controller_Router_Route
			(
			"/shops/edit/:guid",
			array
			("controller" => "shops",
			"action" => "edit") ));
			
			//sales
$Router->addRoute("delshopss",
			new Zend_Controller_Router_Route
			(
			"/shops/delete/:guid",
			array
			("controller" => "shops",
			"action" => "delete") ));
			
			//sales
$Router->addRoute("createpassword",
			new Zend_Controller_Router_Route
			(
			"/member/create-password/:guid",
			array
			("controller" => "account",
			"action" => "create-password") ));
			
			//sales
$Router->addRoute("memberdetails",
			new Zend_Controller_Router_Route
			(
			"/member/details/:accountno",
			array
			("controller" => "member",
			"action" => "details") ));
			
			
			
			